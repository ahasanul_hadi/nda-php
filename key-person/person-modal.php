<?php

include("./database/db_connection.php");

$query="select * from key_persons_info where isDeleted=0 and isPublished=1 order by display_order ASC";
$result=$dbcon->query($query);
$person_size=mysqli_num_rows($result);

?>
<!-- Modal -->


<?php
$row=mysqli_fetch_array($result);
do{
    ?>

    <!-- The Modal -->
    <div class="modal" id="mod<?php echo $row['display_order']?>" >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title"><?php echo $row["name"]?> </h4>&nbsp; &nbsp;
                    <i class="pt-1"><?php echo $row["designation"]?></i>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <p><?php echo $row["description"]?></p>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>

<?php }while($row=mysqli_fetch_assoc($result));

$dbcon->close();
?>


