<?php

include("./database/db_connection.php");

$query="select * from key_persons_info where isDeleted=0 and isPublished=1 order by display_order ASC";
$result=$dbcon->query($query);
$person_size=mysqli_num_rows($result);

?>

<div class="donation_area">
    <div class="container">
        <div class="row donation_inner">



                <?php
                $row=mysqli_fetch_array($result);
                do{
                ?>
                    <div class="col-lg-<?php echo 12/$person_size?>">
                        <a href="#" data-toggle="modal" data-target="#mod<?php echo $row['display_order']?>">
                            <div class="dontation_item text-center" style=" background: <?php echo  $row['color_code'] ?>">
                                <div class="media">
                                    <div class="media-body">
                                        <h5><?php echo $row["name"]?> </h5>
                                        <p><?php echo $row["designation"]?></p>
                                    </div>

                                </div>
                                <img  height="120" width="120" class="img-responsive " style="border-radius: 50%" src="<?php echo './temp/'.$row["image_path"]?>" alt="">
                                <br>
                            </div>
                        </a>
                    </div>


                <?php }while($row=mysqli_fetch_assoc($result));

                $dbcon->close();
                ?>






        </div>
    </div>
</div>