-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 12, 2018 at 02:14 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nea_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `dtree_adminusers`
--

CREATE TABLE `dtree_adminusers` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'active',
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtree_adminusers`
--

INSERT INTO `dtree_adminusers` (`id`, `username`, `password`, `status`, `created_date`) VALUES
(1, 'admin', 'admin', 'active', '2015-08-12');

-- --------------------------------------------------------

--
-- Table structure for table `dtree_applicationarchitecture`
--

CREATE TABLE `dtree_applicationarchitecture` (
  `id` bigint(20) NOT NULL,
  `node_id` bigint(20) NOT NULL,
  `Parent_id` int(11) NOT NULL,
  `Node_name` text NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `uploads` varchar(500) NOT NULL,
  `homeFlag` int(11) NOT NULL,
  `fileDetails` longtext NOT NULL,
  `standard` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Content of Application Architecture';

--
-- Dumping data for table `dtree_applicationarchitecture`
--

INSERT INTO `dtree_applicationarchitecture` (`id`, `node_id`, `Parent_id`, `Node_name`, `description`, `uploads`, `homeFlag`, `fileDetails`, `standard`) VALUES
(1, 0, 0, 'Application Architecture', '', 'uploads/Application Architecture/Images/application_home.jpg', 1, 'uploads/Application Architecture/Content/application_architecture.pdf', ''),
(2, 2, 1, 'Presentation Layer', 'The presentation layer manages the interaction with users and other systems. The presentation layer is composed of the following components: <br/> <ul style=\"margin-left:5%\"><li style=\"list-style:circle\">Users Devices  </li><li style=\"list-style:circle\">Access Channels  </li><li style=\"list-style:circle\">Business application and service layer</li></ul>', 'uploads/Application Architecture/Images/application_presentation.jpg', 0, 'uploads/Application Architecture/Content/presentation_layer.pdf', 'uploads/Application Architecture/Standards/Std_Presentation Layer.pdf'),
(3, 3, 1, 'Business Application Layer', 'Business application and service layer encompasses all the business domain logic, rules, data etc. associated with the application. The components of the business application and service layer are:<br/> <ul style=\"margin-left:5%\"><li style=\"list-style:circle\">Application facade</li><li style=\"list-style:circle\">Service Delivery Platform</li><li style=\"list-style:circle\">Adaptors</li></ul>', 'uploads/Application Architecture/Images/application_business.jpg', 0, 'uploads/Application Architecture/Content/business_layer.pdf', 'uploads/Application Architecture/Standards/Std_Business Layer.pdf'),
(4, 4, 1, 'Data Layer', 'Data and information management layer defines the key components that support the access, storage, backup, replication and representation of data. The layer delivers information management services as well through enabling components like data mining and analytics.', 'uploads/Application Architecture/Images/application_data.jpg', 0, 'uploads/Application Architecture/Content/data_layer.pdf', 'uploads/Application Architecture/Standards/Std_Data Layer.pdf'),
(5, 5, 1, 'Development Tools Layer', 'The Development tools layer consists of modelling, scripting and testing tools which support the designers, developers and tester to execute their respective tasks to meet the business requirements.', 'uploads/Application Architecture/Images/application_development.jpg', 0, 'uploads/Application Architecture/Content/development_layer.pdf', 'uploads/Application Architecture/Standards/Std_Development Layer.pdf'),
(6, 6, 1, 'Infrastructure Layer', 'The infrastructure management layer provides the software applications and tools required to manage and monitor the underlying infrastructure', 'uploads/Application Architecture/Images/application_infrastructure.jpg', 0, 'uploads/Application Architecture/Content/infrastructure_layer.pdf', 'uploads/Application Architecture/Standards/Std_Infrastructure Layer.pdf'),
(7, 7, 1, 'Application Security Layer', ' This section provides an overview of elements that constitute the security layer of the system. The layer has been covered in detail in the security architecture.', 'uploads/Application Architecture/Images/application_security.jpg', 0, 'uploads/Application Architecture/Content/security_layer.pdf', 'uploads/Application Architecture/Standards/Std_Security Layer.pdf'),
(8, 8, 1, 'Exception handling and log management Layer', 'The exception handling and log management layer talks about management of exception handling process and maintenance of required logs in the system for a defined period as per the business requirements', 'uploads/Application Architecture/Images/application_exception.jpg', 0, 'uploads/Application Architecture/Content/exception_layer.pdf', 'uploads/Application Architecture/Standards/Std_Exception Layer.pdf'),
(9, 9, 1, 'Integration Layer', 'Integration layer addresses the interconnection of all the layers and also with external disparate systems. Integration of these layers provides Government entities and other stakeholders with aggregated, secured and consumable information to assist in execution of services. The integration architecture discusses the topic in detail', 'uploads/Application Architecture/Images/application_integration.jpg', 0, 'uploads/Application Architecture/Content/integration_layer.pdf', 'uploads/Application Architecture/Standards/Std_Integration Layer.pdf'),
(10, 10, 0, 'Service Delivery Platform', '', 'uploads/Application Architecture/Images/application_service.jpg', 0, 'uploads/Application Architecture/Content/sdp.pdf', '');

-- --------------------------------------------------------

--
-- Table structure for table `dtree_applicationarchitecture_standards`
--

CREATE TABLE `dtree_applicationarchitecture_standards` (
  `id` bigint(20) NOT NULL,
  `node_id` bigint(20) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `parent_node` int(11) NOT NULL,
  `standard_id` varchar(100) NOT NULL,
  `node_name` text NOT NULL,
  `component` varchar(1000) NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `clasification` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dtree_applicationarchitecture_standards`
--

INSERT INTO `dtree_applicationarchitecture_standards` (`id`, `node_id`, `parent_id`, `parent_node`, `standard_id`, `node_name`, `component`, `description`, `clasification`) VALUES
(1, 0, 0, 0, '', 'Software development lifecycle', '', '', ''),
(2, 2, 1, 0, '', 'Selection of Software Development Lifecycle', '', '', ''),
(3, 3, 0, 2, 'ARM_SDLC_1', 'Selection of Software Development Lifecycle', '', 'Project heads should define the SDLC model from either Waterfall or iterative ', 'Mandatory '),
(4, 4, 0, 2, 'ARM_SDLC_2', 'Selection of Software Development Lifecycle', '', 'Selection and use of one application development methodology for the entire duration of the project', 'Mandatory '),
(5, 5, 0, 2, 'ARM_SDLC_3', 'Selection of Software Development Lifecycle', '', 'To change the selected methodology, a proper change request procedure should be followed', 'Mandatory '),
(6, 6, 0, 2, 'ARM_SDLC_4', 'Selection of Software Development Lifecycle', '', 'Follow ISO/IEC/IEEE 24765 standard for systems and software engineering', 'Recommended'),
(7, 7, 0, 2, 'ARM_SDLC_5', 'Selection of Software Development Lifecycle', '', 'Follow IEEE standard 12207 for software life cycle processes', 'Recommended'),
(8, 8, 0, 2, 'ARM_SDLC_6', 'Selection of Software Development Lifecycle', '', 'Follow IEEE standard 1517 to reuse processes', 'Recommended'),
(9, 9, 1, 0, '', 'Requirement elicitation', '', '', ''),
(10, 10, 0, 9, 'ARM_REQ_1', 'Requirement elicitation', '', 'The project team must gather business and system requirements', 'Mandatory '),
(11, 11, 0, 9, 'ARM_REQ_2', 'Requirement elicitation', '', 'The project team must establish and document business requirements', 'Mandatory '),
(12, 12, 0, 9, 'ARM_REQ_3', 'Requirement elicitation', '', 'A requirement should be traceable back-ward to requirements and the stakeholders that motivated it', 'Mandatory '),
(13, 13, 0, 9, 'ARM_REQ_4', 'Requirement elicitation', '', 'On successful completion a sign-off must be obtained for requirements and design document', 'Mandatory '),
(14, 14, 1, 0, '', 'Software design', '', '', ''),
(15, 15, 0, 14, 'ARM_SDD_1', 'Software design', '', 'Project team must follow IEEE standard 1069 for Information technology - system design', 'Mandatory '),
(16, 16, 0, 14, 'ARM_SDD_2', 'Software design', '', 'The project team must document the software design as per IEEE 1016', 'Mandatory '),
(17, 17, 0, 14, 'ARM_SDD_3', 'Software design', '', 'Follow ISO/IEC 42010 for architecture description', 'Recommended'),
(18, 18, 0, 14, 'ARM_SDD_4', 'Software design', '', 'Project team should use notations for static and dynamic views', 'Recommended'),
(19, 19, 0, 14, 'ARM_SDD_5', 'Software design', '', 'On successful completion a sign-off must be obtained for requirements and design document', 'Mandatory'),
(20, 20, 1, 0, '', 'Coding standards', '', '', ''),
(21, 21, 0, 20, 'ARM_COS_1', 'Coding standards', '', 'Select the programming language appropriately to meet the documented requirements of the system', 'Mandatory'),
(22, 22, 0, 20, 'ARM_COS_2', 'Coding standards', '', 'Indent code for better readability ', 'Mandatory'),
(23, 23, 0, 20, 'ARM_COS_3', 'Coding standards', '', 'Establish a maximum line length for comments and code to avoid horizontal scrolling of editor window', 'Mandatory'),
(24, 24, 0, 20, 'ARM_COS_4', 'Coding standards', '', 'Use space after each comma, operators, values and arguments', 'Mandatory'),
(25, 25, 0, 20, 'ARM_COS_5', 'Coding standards', '', 'Break large, complex sections of code into smaller comprehensible modules/ functions', 'Recommended'),
(26, 26, 0, 20, 'ARM_COS_6', 'Coding standards', '', 'Arrange and separate source code between files', 'Recommended'),
(27, 27, 0, 20, 'ARM_COS_7', 'Coding standards', '', 'Choose and stick to naming convention', 'Recommended'),
(28, 28, 0, 20, 'ARM_COS_8', 'Coding standards', '', 'Avoid elusive names that are open to subjective interpretation', 'Recommended'),
(29, 29, 0, 20, 'ARM_COS_9', 'Coding standards', '', 'Do not include class names in the name of class properties', 'Recommended'),
(30, 30, 0, 20, 'ARM_COS_10', 'Coding standards', '', 'Use the verb-noun method for naming routines', 'Recommended'),
(31, 31, 0, 20, 'ARM_COS_11', 'Coding standards', '', 'Append computation qualifiers (Avg, Sum, Min, Max, Index) to the end of a variable name where appropriate', 'Recommended'),
(32, 32, 0, 20, 'ARM_COS_12', 'Coding standards', '', 'Use customary opposite pairs in variable names', 'Recommended'),
(33, 33, 0, 20, 'ARM_COS_13', 'Coding standards', '', 'use mixed-case formatting to simplify reading', 'Recommended'),
(34, 34, 0, 20, 'ARM_COS_14', 'Coding standards', '', 'Boolean variable names should contain Is which implies Yes/No or True/False values', 'Recommended'),
(35, 35, 0, 20, 'ARM_COS_15', 'Coding standards', '', 'Avoid using terms such as Flag when naming status variables, which differ from Boolean variables in that they may have more than two possible values', 'Recommended'),
(36, 36, 0, 20, 'ARM_COS_16', 'Coding standards', '', 'Even for a short-lived variable that may appear in only a few lines of code, still use a meaningful name. Use single-letter variable names, such as i, or j, for short-loop indexes only.', 'Recommended'),
(37, 37, 0, 20, 'ARM_COS_17', 'Coding standards', '', 'Develop a list of standard prefixes for the project to help developers consistently name variables', 'Recommended'),
(38, 38, 0, 20, 'ARM_COS_18', 'Coding standards', '', 'For variable names, include notation that indicates the scope of the variable', 'Recommended'),
(39, 39, 0, 20, 'ARM_COS_19', 'Coding standards', '', 'Constants should be all uppercase with underscores between words', 'Recommended'),
(40, 40, 0, 20, 'ARM_COS_20', 'Coding standards', '', 'Wrap built-in functions and third-party library functions with\r\nyour own wrapper functions\r\n', 'Recommended'),
(41, 41, 0, 20, 'ARM_COS_21', 'Coding standards', '', 'Report error message and recover or fail gracefully', 'Recommended'),
(42, 42, 0, 20, 'ARM_COS_22', 'Coding standards', '', 'Provide useful error messages', 'Recommended'),
(43, 43, 0, 20, 'ARM_COS_23', 'Coding standards', '', 'When modifying code, always keep the commenting around it up to date', 'Recommended'),
(44, 44, 0, 20, 'ARM_COS_24', 'Coding standards', '', 'At the beginning of every routine, it is helpful to provide standard, boilerplate comments, indicating the routine\'s purpose, assumptions, and limitations', 'Recommended'),
(45, 45, 0, 20, 'ARM_COS_25', 'Coding standards', '', 'Avoid adding comments at the end of a line of code', 'Recommended'),
(46, 46, 0, 20, 'ARM_COS_26', 'Coding standards', '', 'To conserve resources, be selective in the choice of data type to ensure the size of a variable is not excessively large.', 'Recommended'),
(47, 47, 0, 20, 'ARM_COS_27', 'Coding standards', '', 'Keep the scope of variables as small as possible to avoid confusion and to ensure maintainability', 'Recommended'),
(48, 48, 0, 20, 'ARM_COS_28', 'Coding standards', '', 'When writing classes, avoid the use of public variables. Instead, use procedures to provide a layer of encapsulation and also to allow an opportunity to validate value changes.', 'Recommended'),
(49, 49, 0, 20, 'ARM_COS_29', 'Coding standards', '', 'Do not open data connections using a specific user\'s credentials. Connections that have been opened using such credentials cannot be pooled and reused, thus losing the benefits of connection pooling.', 'Recommended'),
(50, 50, 1, 0, '', 'Testing standards', '', '', ''),
(51, 51, 0, 50, 'ARM_TST_1', 'Testing standards', '', 'Follow ISO/IEC/IEEE standard 29119 for software testing', 'Mandatory'),
(52, 52, 0, 50, 'ARM_TST_2', 'Testing standards', '', 'Follow ISO/IEC standard 15288 and 12207 for system engineering standards include process for verification and validation', 'Recommended'),
(53, 53, 0, 50, 'ARM_TST_3', 'Testing standards', '', 'Follow IEEE 1008, BS 7925 standard for testing', 'Recommended'),
(54, 54, 0, 50, 'ARM_TST_4', 'Testing standards', '', 'Follow IEEE 829, 1028 for software review techniques', 'Recommended'),
(55, 55, 1, 0, '', 'Software maintenance', '', '', ''),
(56, 56, 0, 55, 'ARM_SOM_1', 'Software maintenance', '', 'Follow ISO/IEC standard 14764 for software maintenance', 'Mandatory'),
(57, 57, 0, 55, 'ARM_SOM_2', 'Software maintenance', '', 'Follow IEEE standard 1219 and 14764 for process of software maintenance', 'Mandatory'),
(58, 0, 0, 0, '', 'Application architecture reference model layers', '', '', ''),
(59, 59, 58, 0, '', 'Website guidelines', '', '', ''),
(60, 60, 0, 59, 'WEB.DES.001', 'Website guidelines', 'Common Requirements', 'Website should be registered under \'gov.bd\' domain', 'Mandatory'),
(61, 61, 0, 59, 'WEB.DES.002', 'Website guidelines', 'Common Requirements', 'The link to other websites and portal should open in a new tab or a new window', 'Mandatory'),
(62, 62, 0, 59, 'WEB.DES.003', 'Website guidelines', 'Common Requirements', 'Content should be free from spelling and grammatical errors', 'Mandatory'),
(63, 63, 0, 59, 'WEB.DES.004', 'Website guidelines', 'Common Requirements', 'The content should not be discriminative/ offensive', 'Mandatory'),
(64, 64, 0, 59, 'WEB.DES.005', 'Website guidelines', 'Common Requirements', 'A policy should be prevalent in department for review of content to be published on website', 'Mandatory'),
(65, 65, 0, 59, 'WEB.DES.006', 'Website guidelines', 'Common Requirements', 'The website should provide option for content translated in atleast English language', 'Mandatory');

-- --------------------------------------------------------

--
-- Table structure for table `dtree_businessarchitecture`
--

CREATE TABLE `dtree_businessarchitecture` (
  `id` bigint(20) NOT NULL,
  `node_id` bigint(20) NOT NULL,
  `Parent_id` int(11) NOT NULL,
  `Node_name` text NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `uploads` varchar(500) NOT NULL,
  `homeFlag` int(11) NOT NULL,
  `fileDetails` longtext NOT NULL,
  `standard` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Content of Business Architecture';

--
-- Dumping data for table `dtree_businessarchitecture`
--

INSERT INTO `dtree_businessarchitecture` (`id`, `node_id`, `Parent_id`, `Node_name`, `description`, `uploads`, `homeFlag`, `fileDetails`, `standard`) VALUES
(1, 0, 0, 'Business Architecture', 'Enterprise architecture defines the contours of IT architecture design for the following areas: Business, data, application, infrastructure, mobility, integration etc. Sound knowledge of the business architecture is the pre-requisite for architecture work in other domains such as data, application and technology and is thus the foremost architecture activity that is required to be taken up.', 'uploads/Business Architecture/Images/Business_home.jpg', 1, '', ''),
(2, 2, 1, 'Core Functions', 'Core functions are those that are integral or specific to the ministry or department’s functions. These are the services that are intrinsic to the development of Bangladesh and are the basic touch-points for interfacing of the citizens with the GoB', 'uploads/Business Architecture/Images/Business_core.jpg', 0, 'uploads/Business Architecture/Content/Business_core.pdf', 'uploads/Business Architecture/Standards/Business_standards.pdf'),
(3, 3, 1, 'Ancillary Functions', 'These services though important however are not required by a citizen on a daily basis. However, they are essential for the development of Bangladesh. These components are associated with the citizens of the country.', 'uploads/Business Architecture/Images/Business_Ancillary.jpg', 0, 'uploads/Business Architecture/Content/Business_ancillary.pdf', 'uploads/Business Architecture/Standards/Business_standards.pdf'),
(4, 4, 1, 'Business Support Functions', 'These services form the fundamental backbone of internal operations of any ministry or department. These internal services determine the efficiency of delivery of the citizen centric services by the ministry or department', 'uploads/Business Architecture/Images/Business_support.jpg', 0, 'uploads/Business Architecture/Content/Business_support.pdf', 'uploads/Business Architecture/Standards/Business_standards.pdf'),
(5, 5, 0, 'Enabling Functions', 'The enabling services are essentially the IT services that automate the core, ancillary and business support functions provided by the GoB to its citizens or employees', 'uploads/Business Architecture/Images/Business_enabling.jpg', 0, 'uploads/Business Architecture/Content/Business_enabling.pdf', 'uploads/Business Architecture/Standards/Business_standards.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `dtree_businessarchitecture_standards`
--

CREATE TABLE `dtree_businessarchitecture_standards` (
  `id` bigint(20) NOT NULL,
  `node_id` bigint(20) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `parent_node` int(11) NOT NULL,
  `standard_id` varchar(100) NOT NULL,
  `node_name` text NOT NULL,
  `component` varchar(1000) NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `clasification` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dtree_businessarchitecture_standards`
--

INSERT INTO `dtree_businessarchitecture_standards` (`id`, `node_id`, `parent_id`, `parent_node`, `standard_id`, `node_name`, `component`, `description`, `clasification`) VALUES
(1, 0, 0, 0, '', 'Business Architecture Standards', '', '', ''),
(2, 2, 0, 1, 'BUS.PRC.001', 'Business Architecture Standards', 'Business Process Modelling Notation (BPMN)', 'BPMN defines a Business Process Diagram (BPD), which is based on a flowcharting technique tailored for creating graphical models of business process operations. Modelling in BPMN uses set of diagrams with a small set of graphical elements to assist business users, as well as developers, to understand the flow and the process.', 'Mandatory'),
(3, 3, 0, 1, 'BUS.PRC.002', 'Business Architecture Standards', 'Business Process Execution Language (BPEL)', 'This is an XML based language which is used to define enterprise business processes with web services. The key objective of BPEL is to standardize the format of business process flow definition so that the departments can work together seamlessly using web services. Therefore, BPEL focuses on web service interfaces specifically. There is no standard graphical notation for BPEL. Instead, BPMN is used as a front end tool to capture BPEL process descriptions.', 'Recommended'),
(4, 4, 0, 1, 'BUS.PRC.003', 'Business Architecture Standards', 'Unified Modeling Language (UML)', 'It is a tool which helps in visualizing architectural blue prints such as activities, individual components of a system, interaction of entities, user interface etc.', 'Recommended');

-- --------------------------------------------------------

--
-- Table structure for table `dtree_dataarchitecture`
--

CREATE TABLE `dtree_dataarchitecture` (
  `id` bigint(20) NOT NULL,
  `node_id` bigint(20) NOT NULL,
  `Parent_id` int(11) NOT NULL,
  `Node_name` text NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `uploads` varchar(500) NOT NULL,
  `homeFlag` int(11) NOT NULL,
  `fileDetails` longtext NOT NULL,
  `standard` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Content of Business Architecture';

--
-- Dumping data for table `dtree_dataarchitecture`
--

INSERT INTO `dtree_dataarchitecture` (`id`, `node_id`, `Parent_id`, `Node_name`, `description`, `uploads`, `homeFlag`, `fileDetails`, `standard`) VALUES
(1, 1, 0, 'Data Architecture', 'The data architecture focuses on access, storage and management of data.  It supports the needs and requirements of the application and business layers. The data layer attempts to address issues caused by ’silo-based‘ systems development, where each application maintains a copy of the underlying data and the associated process rules tailor made to suit the local  context and needs. Resolving these issues is central to providing integrated e-Government services and the continuous improvement and adaptation of these services are essential so as to comply with policy changes.', 'uploads/Data_Standard.jpg', 1, 'uploads/data_architecture.pdf', '');

-- --------------------------------------------------------

--
-- Table structure for table `dtree_dataarchitecture_standards`
--

CREATE TABLE `dtree_dataarchitecture_standards` (
  `id` bigint(20) NOT NULL,
  `node_id` bigint(20) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `parent_node` int(11) NOT NULL,
  `standard_id` varchar(100) NOT NULL,
  `node_name` text NOT NULL,
  `component` varchar(1000) NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `clasification` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dtree_dataarchitecture_standards`
--

INSERT INTO `dtree_dataarchitecture_standards` (`id`, `node_id`, `parent_id`, `parent_node`, `standard_id`, `node_name`, `component`, `description`, `clasification`) VALUES
(1, 0, 0, 0, '', 'General data standards', '', '', ''),
(2, 2, 1, 0, '', 'Data management', '', '', ''),
(3, 3, 0, 2, 'DAT.DM.001', 'Data management', 'Data Access Services\r\n', 'Use DBMS that supports JDBC latest version for  java based applications.', 'Recomended'),
(4, 4, 0, 2, 'DAT.DM.002', 'Data management', 'Data Access Services\r\n', 'Use DBMS that supports ODBC for non-Java based applications. As ODBC drivers are implemented by various vendors, it would be advisable to identify DBMS that support the latest stable version of the ODBC.', 'Recomended'),
(5, 5, 0, 2, 'DAT.DM.003', 'Data management', 'Data Query Language<br/>\n\nReference Site : \nwww.iso.org', 'Support for SQL:2003 standards defined in ISO/IEC 9075. SQL:2003 is the fifth revision of SQL used by relational database.', 'Mandatory'),
(6, 6, 0, 2, 'DAT.DM.004', 'Data management', 'Data Query Language<br/>\n\nReference Site : \nwww.iso.org', 'Support for SQL:2008 standards defined in ISO/IEC 9075. SQL:2008 is the latest 2008 revision of SQL used by relational database.', 'Recomended'),
(7, 7, 0, 2, 'DAT.DM.005', 'Data management', 'Data Indexing', 'There is no technical standard for compliance. Please refer to Best Practices for more information.', 'Recomended'),
(8, 8, 0, 2, 'DAT.DM.006', 'Data management', 'Data Tuning', 'There is no technical standard for compliance. Please refer to Best Practices for more information.', 'Recomended'),
(9, 9, 0, 2, 'DAT.DM.007', 'Data management', 'Data Clustering', 'There is no technical standard for compliance. Please refer to Best Practices for more information.', 'Recomended'),
(10, 10, 0, 2, 'DAT.DM.008', 'Data management', 'Data Integrity', 'There is no technical standard for compliance. Please refer to Best Practices for more information.', 'Recomended'),
(11, 11, 1, 0, '', 'Data design', '', '', ''),
(12, 12, 0, 11, 'DAT.DD.001', 'Data design', 'Data Modelling', 'Use one of the following notations for data modelling:<br/>\n(a) Unified Modelling Language (UML)<br/>\n(b) Barker\'s Notation<br/>\n(c) Information Engineering.\n', 'Recomended'),
(13, 13, 0, 11, 'DAT.DD.002', 'Data design', 'Internationalisation', 'Use Unicode standard to support the worldwide.', 'Recomended'),
(14, 14, 1, 0, '', 'Extract, Transform, Load (ETL)', '', '', ''),
(15, 15, 0, 14, 'DAT.ETL.001', 'Extract, Transform, Load (ETL)', 'ETL', 'ETL tools should be used in scenarios where large amounts of data need to be moved, transformed, enriched, and/or merged from multiple data sources to a target source. An example of this is the loading of data from source systems into a data warehouse', 'Recomended'),
(16, 16, 0, 14, 'DAT.ETL.002', 'Extract, Transform, Load (ETL)', 'ETL', 'ETL processes should be scheduled so that they do not impact the operations and end users of the source systems they are extracting from', 'Recomended'),
(17, 17, 0, 14, 'DAT.ETL.003', 'Extract, Transform, Load (ETL)', 'ETL', 'The ETL process should encourage to move the data from the source to the ETL environment quickly and should access the source only once. The target architecture should ensure re-use of a single data copy from production sources to minimize resource utilization on the source system ', 'Recomended'),
(18, 18, 0, 14, 'DAT.ETL.004', 'Extract, Transform, Load (ETL)', 'ETL', 'The artefacts of ETL processes (e.g., scripts, SQL code, data mappings, etc.) should be kept in a repository and managed so that lineage of the data produced from those processes is traceable', 'Recomended'),
(19, 19, 0, 14, 'DAT.ETL.005', 'Extract, Transform, Load (ETL)', 'ETL', 'ETL processes should encourage use of a centralized metadata repository to ensure data quality and integrity. ', 'Recomended'),
(20, 20, 0, 14, 'DAT.ETL.006', 'Extract, Transform, Load (ETL)', 'ETL', 'ETL processes should provision for a facility to perform standard centralized data quality checks with required and optional checks which may be decided by the target system', 'Recomended'),
(21, 21, 0, 14, 'DAT.ETL.007', 'Extract, Transform, Load (ETL)', 'ETL', 'ETL processes should provision for a storage mechanism for clean data thus eliminating the need for new processes to re-source data recheck values or re-compute derived values.', 'Recomended'),
(22, 22, 1, 0, '', 'Metadata Management', '', '', ''),
(23, 23, 0, 22, 'DAT.MM.001', 'Metadata Management', 'Metadata Management', 'Element : Element description', 'Mandatory'),
(24, 24, 0, 22, 'DAT.MM.002', 'Metadata Management', 'Metadata Management', 'Creator : Person or organisation primarily\nresponsible for creating the\nintellectual content of the\nresource-e.g., authors in the case\nof written documents, and artists,\nphotographers, etc. in the case of visual resources\n', 'Mandatory'),
(25, 25, 0, 22, 'DAT.MM.003', 'Metadata Management', 'Metadata Management', 'Publisher : The entity (e.g., agency, including\r\nunit/branch/section) responsible for making the resource available in its present form, such as a publishing house, a university department, or a corporate entity.\r\n', 'Mandatory'),
(26, 26, 0, 22, 'DAT.MM.005', 'Metadata Management', 'Metadata Management', 'Rights Management : A rights management statement or an identifier that links to a rights\r\nmanagement statement.\r\n', 'Mandatory'),
(27, 27, 0, 22, 'DAT.MM.006', 'Metadata Management', 'Metadata Management', 'Title : The name given to the resource, usually by the creator or publisher.', 'Mandatory'),
(28, 28, 0, 22, 'DAT.MM.007', 'Metadata Management', 'Metadata Management', 'Subject : The topic of the resource. Typically,\r\nthis will be expressed as keywords or phrases that describe the subject or content of the resource. Controlled vocabularies and\r\nformal classification schemes are encouraged.\r\n', 'Mandatory'),
(29, 29, 0, 22, 'DAT.MM.008', 'Metadata Management', 'Metadata Management', 'Date : A date associated with the creation or availability of the resource.', 'Mandatory'),
(30, 30, 0, 22, 'DAT.MM.009', 'Metadata Management', 'Metadata Management', 'Identifier : A string or number used to uniquely\r\nidentify the resource. Examples for networked resources include URLs, Purls, and URNs. ISBN or other formal names can be used.\r\n', 'Mandatory'),
(31, 31, 0, 22, 'DAT.MM.010', 'Metadata Management', 'Metadata Management', 'Description : A textual description of the content of the resource, including abstracts in the case of document-like objects or content descriptions in the case of visual resources.', 'Mandatory'),
(32, 32, 0, 22, 'DAT.MM.011', 'Metadata Management', 'Metadata Management', 'Source : The work, either print or electronic,\r\nfrom which this object is derived (if applicable). Source is not applicable if the present resource is in its original form.\r\n', 'Mandatory'),
(33, 33, 0, 22, 'DAT.MM.012', 'Metadata Management', 'Metadata Management', 'Language : The language of the intellectual\r\ncontent of the resource.\r\n', 'Mandatory'),
(34, 34, 0, 22, 'DAT.MM.013', 'Metadata Management', 'Metadata Management', 'Relation : Relationship to other resources-e.g.,\r\nimages in a document, chapters in a\r\nbook, or items in a collection\r\n', 'Mandatory'),
(35, 35, 0, 22, 'DAT.MM.014', 'Metadata Management', 'Metadata Management', 'Coverage : Spatial locations and temporal\r\nduration characteristic of the resource.\r\n', 'Mandatory'),
(36, 36, 0, 22, 'DAT.MM.015', 'Metadata Management', 'Metadata Management', 'Type : The category of the resource, such\r\nas home page, novel, poem, working\r\npaper, technical report, essay, or\r\ndictionary.\r\n', 'Mandatory'),
(37, 37, 0, 22, 'DAT.MM.016', 'Metadata Management', 'Metadata Management', 'Format : The data format of the resource, used to identify the software and possibly hardware that might be needed to display or operate the\r\nresource-e.g., postscript, HTML, TXT, JPEG, or XML.\r\n', 'Mandatory'),
(38, 38, 0, 0, '', 'Technical data standards', '', '', ''),
(39, 39, 38, 0, '', 'Data Security', '', '', ''),
(40, 40, 0, 39, 'DAT.DS.001', 'Data Security', 'Encryption', 'Use cryptographic techniques for encryption\r\nof sensitive data. The reference standards for\r\ncryptography include Triple Data Encryptions Standard (3DES), Advance Encryption Standard (AES).\r\n', 'Recommended'),
(41, 41, 0, 39, 'DAT.DS.002', 'Data Security', 'Network', 'Databases should not be accessible directly from external network (non-government network).', 'Mandatory'),
(42, 42, 0, 39, 'DAT.DS.003', 'Data Security', 'Database', 'Use RDBMS with security controls to ensure aggregation (value of disclosed data) and inference (confidentiality).', 'Mandatory'),
(43, 43, 0, 39, 'DAT.DS.004', 'Data Security', 'Database', 'Use RDBMS that supports the following security controls:<br/>\r\nData access as an intended privilege<br/>\r\n(b) Key management and encryption<br/>\r\n(c) Integrity constrains such as domain constraints,\r\nattribute constraints, relation constraints, and\r\ndatabase constraints<br/>\r\n(d) High availability implementation, backup,\r\nrestoration and data replication<br/>\r\n(e) Database log and policy enforcement\r\n', 'Mandatory'),
(44, 44, 0, 39, 'DAT.DS.005', 'Data Security', 'Data Destruction ', 'Data destruction shall be done using degaussing\r\n(NIST 800-88 guidelines for Media Sanitisation), data overwriting (Bruce chneier algorithm, DOD 5220.22-M, Peter Gutmann Secure Deletion) and physical\r\n', 'Recommended'),
(45, 45, 38, 0, '', 'Data Storage, Backup and Archival', '', '', ''),
(46, 46, 0, 45, 'DAT.DBA.001', 'Data Storage, Backup and Archival', 'Data Storage, Backup and Archival', 'Data Archiving shall support integrity checking through hashing, audit logging and regulatory compliance.', 'Mandatory'),
(47, 47, 0, 45, 'DAT.DBA.002', 'Data Storage, Backup and Archival', 'Data Storage, Backup and Archival', 'Strict security policies should be established for archived data to prevent unauthorised access and data loss.', 'Mandatory'),
(48, 48, 0, 45, 'DAT.DBA.003', 'Data Storage, Backup and Archival', 'Data Storage, Backup and Archival', 'Use ISO 15489-1 for records management.', 'Recommended'),
(49, 49, 0, 45, 'DAT.DBA.004', 'Data Storage, Backup and Archival', 'Data Storage, Backup and Archival', 'Use the Dublin Core metadata element set for resource description based on ISO 15836.', 'Recommended'),
(50, 50, 0, 45, 'DAT.DBA.005', 'Data Storage, Backup and Archival', 'Data Storage, Backup and Archival', 'Use portable document format for document\r\nmanagement based on ISO 32000-1.\r\n', 'Recommended'),
(51, 51, 0, 45, 'DAT.DBA.006', 'Data Storage, Backup and Archival', 'Data Storage, Backup and Archival', 'Use ISO/TR 18492 for long-term preservation of\r\nelectronic document-based information.\r\n', 'Recommended'),
(52, 52, 0, 45, 'DAT.DBA.007', 'Data Storage, Backup and Archival', 'Data Storage, Backup and Archival', 'Use Open Archival Information System (OAIS) to\r\nestablish a system for archiving information for both digitalized and physical. This framework is based on ISO 14721.\r\n', 'Recommended'),
(53, 53, 38, 0, '', 'Metadata, Spatial data Management, Enterprise Schema and BI', '', '', ''),
(54, 54, 0, 53, 'DAT.MSEB.001', 'Metadata, Spatial data Management, Enterprise Schema and BI', 'Metadata, Spatial data Management, Enterprise Schema and BI', 'Use XML Schemas 1.0 and above to manage and overall Enterprise Schema.', 'Mandatory'),
(55, 55, 0, 53, 'DAT.MSEB.002', 'Metadata, Spatial data Management, Enterprise Schema and BI', 'Metadata, Spatial data Management, Enterprise Schema and BI', 'Use Metadata Object Facility (MOF) to define, manipulate and integrate metadata and data in a platform independent manner.', 'Mandatory'),
(56, 56, 0, 53, 'DAT.MSEB.003', 'Metadata, Spatial data Management, Enterprise Schema and BI', 'Metadata, Spatial data Management, Enterprise Schema and BI', 'Support Resource Description Framework (RDF) framework for describing and interchanging\r\nmetadata based on resource, properties and statements definitions.\r\n', 'Recommended'),
(57, 57, 0, 53, 'DAT.MSEB.004', 'Metadata, Spatial data Management, Enterprise Schema and BI', 'Metadata, Spatial data Management, Enterprise Schema and BI', 'Support Common Warehouse Metamodel (CWM) to enable interchange of warehouse and BI metadata between warehouse tools, warehouse platforms and warehouse Metadata repositories in distributed heterogeneous environments.', 'Recommended'),
(58, 58, 0, 53, 'DAT.MSEB.005', 'Metadata, Spatial data Management, Enterprise Schema and BI', 'Metadata, Spatial data Management, Enterprise Schema and BI', 'Support Common Warehouse Metamodel Metadata\r\nInterchange Patterns to add semantic context to the interchange of Metadata in terms of recognised sets of objects or object patterns.\r\n', 'Recommended'),
(59, 59, 0, 53, 'DAT.MSEB.006', 'Metadata, Spatial data Management, Enterprise Schema and BI', 'Metadata, Spatial data Management, Enterprise Schema and BI', 'Use the set of standards produced by ISO/TC 211 that supports the understanding and usage of geographic information.', 'Recommended'),
(60, 60, 0, 53, 'DAT.MSEB.007', 'Metadata, Spatial data Management, Enterprise Schema and BI', 'Metadata, Spatial data Management, Enterprise Schema and BI', 'Support Open Geospatial Consortium (OpenGIS)\r\nSimple Feature that provides a way for application to access spatial data in RDBMS.\r\nThere are three standards available – CORBA, SQL and OLE/COM.\r\n', 'Recommended'),
(61, 61, 0, 53, 'DAT.MSEB.008', 'Metadata, Spatial data Management, Enterprise Schema and BI', 'Metadata, Spatial data Management, Enterprise Schema and BI', 'Use Open GIS Geography Markup Language Encoding Standard (GML 2, GML 3) for transfer and storage of geographic information.', 'Recommended'),
(62, 62, 0, 53, 'DAT.MSEB.009', 'Metadata, Spatial data Management, Enterprise Schema and BI', 'Metadata, Spatial data Management, Enterprise Schema and BI', 'Support Open GIS Web Map Service (WMS), Web\r\nFeature Services (WFS) and Web Coverage Service\r\n(WCS) specifications which specify protocols that\r\nprovide uniform access by HTML clients to maps\r\nrendered by WMS enabled map servers on the internet.\r\n', 'Recommended'),
(63, 63, 0, 53, 'DAT.MSEB.010', 'Metadata, Spatial data Management, Enterprise Schema and BI', 'Metadata, Spatial data Management, Enterprise Schema and BI', 'Support Open GIS Catalogue Services Interface\r\nStandards (CAT) to publish and search collections of descriptive information (metadata) about geospatial data, services and related resources.\r\n', 'Recommended'),
(64, 64, 0, 53, 'DAT.MSEB.011', 'Metadata, Spatial data Management, Enterprise Schema and BI', 'Metadata, Spatial data Management, Enterprise Schema and BI', 'Support Open GIS Keyhole Markup Language (KML)Service for geographic visualisation, including\r\nannotation of maps and images.\r\n', 'Recommended'),
(65, 65, 0, 0, '', 'Industry data exchange standards', '', '', ''),
(66, 66, 0, 65, 'DAT.IDES.001', 'Industry data exchange standards', 'Data Exchange', 'Use Extensible Markup Language (XML 1.0 or XML1.1) as a preferred data exchange standard.', 'Recommended'),
(67, 67, 0, 65, 'DAT.IDES.002', 'Industry data exchange standards', 'Data Exchange', 'Support the following standards for exchange of\r\ntextual data:<br/>\r\n(a) Extensible Markup Language (XML 1.0 or XML 1.1) for most applications<br/>\r\n(b) Support Comma Separated Value (CSV) for legacy applications\r\n', 'Recommended'),
(68, 68, 0, 65, 'DAT.IDES.003', 'Industry data exchange standards', 'Data Exchange', 'Support the following standards for exchange of image data:<br/>\r\n(a) Joint Photographic Experts Group (JPEG) for photography images<br/>\r\n(b) Graphics Interchange Format (GIF) for internet images due to its small size and support for animation<br/>\r\n(c) Tagged Image File Format (TIFF) for scanned Images<br/>\r\n(d) Portable Network Graphic (PNG) for internet images which require increased colour depth compared to GIF', 'Mandatory'),
(69, 69, 0, 65, 'DAT.IDES.004', 'Industry data exchange standards', 'Data Exchange', 'Support the following standards for exchange of video and audio data:<br/>\r\n(a) Moving Pictures Expert Group (MPEG-1 to MPEG-4) for most audio and video applications<br/>\r\n(b) 3rd Generation Partnership Project (3GPP and 3GPP2) for audio and video over 3G mobile Networks\r\n', 'Recommended'),
(70, 70, 0, 65, 'DAT.IDES.005', 'Industry data exchange standards', 'Data Exchange', 'Support the file transfer through client file transfer and Server File transfer – FTP server', 'Recommended'),
(71, 71, 0, 65, 'DAT.IDES.006', 'Industry data exchange standards', 'Data Exchange', 'Web Service Description Language is an XML based interface definition language that is used describing the functionality offered by a web service', 'N.A.'),
(72, 72, 0, 65, 'DAT.IDES.007', 'Industry data exchange standards', 'Data Exchange', 'Web Services Security (WS-Security, WSS) is an extension to SOAP (Simple Object Access protocol) to apply security to Web services', 'N.A.'),
(73, 73, 0, 65, 'DAT.IDES.008', 'Industry data exchange standards', 'Data Exchange', 'Use XML Metadata Interchange (XMI) as a XML\r\nIntegration framework for defining, interchanging, manipulating and integrating XML data and objects.\r\n', 'Recommended'),
(74, 74, 0, 65, 'DAT.IDES.009', 'Industry data exchange standards', 'Data Exchange', 'Use xPath 2.0, an XML path language for selecting nodes from an XML document.', 'Recommended'),
(75, 75, 0, 65, 'DAT.IDES.010', 'Industry data exchange standards', 'Data Exchange', 'Use XQuery 1.0 to design query collections for XML data.', 'Recommended'),
(76, 76, 0, 65, 'DAT.IDES.011', 'Industry data exchange standards', 'Data Exchange', 'Use XSLT 2.0 for transforming XML documents into other XML documents.', 'Recommended'),
(77, 77, 0, 65, 'DAT.IDES.012', 'Industry data exchange standards', 'Data Exchange', 'Message queues and mailboxes are software-engineeringcomponents used for inter-process communication (IPC), or for inter-thread communication within the same process', 'N.A.'),
(78, 78, 0, 65, 'DAT.IDES.013', 'Industry data exchange standards', 'Data Exchange', 'A directory service is a software system that stores, organizes, and provides access to information in a computer operating system\'s directory', 'N.A.');

-- --------------------------------------------------------

--
-- Table structure for table `dtree_egif_standards`
--

CREATE TABLE `dtree_egif_standards` (
  `id` bigint(20) NOT NULL,
  `node_id` bigint(20) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `parent_node` int(11) NOT NULL,
  `standard_id` varchar(100) NOT NULL,
  `node_name` text NOT NULL,
  `component` varchar(1000) NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `clasification` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dtree_egif_standards`
--

INSERT INTO `dtree_egif_standards` (`id`, `node_id`, `parent_id`, `parent_node`, `standard_id`, `node_name`, `component`, `description`, `clasification`) VALUES
(1, 0, 0, 0, '', 'e-GIF standards', '', '', ''),
(2, 2, 1, 0, '', 'Presentation', '', '', ''),
(3, 3, 0, 2, 'EGIF.PRT.001', 'Presentation', '', 'WCAG 2.0 guidelines and associated success criteria should be met by all websites and web portals  (http://www.w3.org/TR/WCAG20/)', 'Recommended'),
(4, 4, 0, 2, 'EGIF.PRT.002', 'Presentation', '', 'W3C web and mobile guidelines and best practices (http://www.w3.org/Mobile/) ', 'Recommended'),
(5, 5, 1, 0, '', 'Business process interoperability', '', '', ''),
(6, 6, 0, 5, 'EGIF.BPI.001', 'Business process interoperability', '', 'UMLv2.3 is a language for specifying, constructing, and documenting the artifacts of software-intensive systems', 'Recommended'),
(7, 7, 0, 5, 'EGIF.BPI.002', 'Business process interoperability', '', 'SoaML extends the unified modeling language (UML) to enable the modeling and design of services within a service-oriented architecture.', 'Recommended'),
(8, 8, 0, 5, 'EGIF.BPI.003', 'Business process interoperability', '', 'BPMN 2.0 provide a notation that is readily understandable by all business users, from the business analysts that create the initial drafts of the processes, to the technical developers responsible for implementing the technology that will perform those processes, and finally, to the business people who will manage and monitor those processes.', 'Recommended'),
(9, 9, 0, 5, 'EGIF.BPI.004', 'Business process interoperability', '', 'BPEL4WS - Business process execution language for web services - a language for the specification of business processes and business interaction protocols.', 'Recommended'),
(10, 10, 1, 0, '', 'Data exchange interoperability', '', '', ''),
(11, 11, 0, 10, 'EGIF.DEI.001', 'Data exchange interoperability', '', 'XML and XML schemas should be used for data integration.', 'Mandatory'),
(12, 12, 0, 10, 'EGIF.DEI.002', 'Data exchange interoperability', '', 'UML, RDF and XML for data modelling and description languages.', 'Recommended'),
(13, 13, 0, 10, 'EGIF.DEI.003', 'Data exchange interoperability', '', 'XSLT v2.0 - XSL Transformations - a language for transforming XML documents into other XML documents.', 'Recommended'),
(14, 14, 0, 10, 'EGIF.DEI.004', 'Data exchange interoperability', '', 'Compliance with JMS for all J2EE MOM.', 'Recommended'),
(15, 15, 0, 10, 'EGIF.DEI.005', 'Data exchange interoperability', '', 'An XML and CSV output should be provided for forms data entry.', 'Mandatory'),
(16, 16, 0, 10, 'EGIF.DEI.006', 'Data exchange interoperability', '', 'ISO/IEC 11179-3:2013 for specification and standardization of data / meta data elements.', 'Recommended'),
(17, 17, 0, 10, 'EGIF.DEI.007', 'Data exchange interoperability', '', 'ANSI HL7 Health Level Seven Standard Version 2.4 - Application Protocol for Electronic Data Interchange in Healthcare Environments.', 'Requires discussion'),
(18, 18, 0, 10, 'EGIF.DEI.008', 'Data exchange interoperability', '', 'ebXML Standard Message Service Specification Version 2.0 for security and reliability extensions to SOAP.', 'Mandatory'),
(19, 19, 0, 10, 'EGIF.DEI.009', 'Data exchange interoperability', '', 'ISO15022 - XML Design rules to support design of message types and specific information flows.', 'Recommended'),
(20, 20, 0, 10, 'EGIF.DEI.010', 'Data exchange interoperability', '', 'UN/EDIFACT - Electronic Data Interchange for Administration, Commerce, and Transport. The United Nations EDI standard.', 'Requires discussion'),
(21, 21, 0, 10, 'EGIF.DEI.011', 'Data exchange interoperability', '', 'XBRL Meta Model v2.1.1 - eXtensible Business Reporting Language - an XML language for business reporting.', 'Recommended'),
(22, 22, 0, 10, 'EGIF.DEI.012', 'Data exchange interoperability', '', 'XMI - XML Metadata Interchange Format. An open information interchange model.', 'Recommended'),
(23, 23, 0, 10, 'EGIF.DEI.013', 'Data exchange interoperability', '', 'XSL v1.0 - eXtensible Stylesheet Language - A family of recommendations for describing stylesheets for XML document transformation and presentation.', 'Recommended'),
(24, 24, 0, 10, 'EGIF.DEI.014', 'Data exchange interoperability', '', 'ER Diagrams - Entity-Relationship diagram - a diagramming notation used in data modeling for relational data bases.', 'Mandatory'),
(25, 25, 0, 10, 'EGIF.DEI.015', 'Data exchange interoperability', '', 'XML schema Parts 0-2:2001 - An XML-based language for defining the structure of XML documents and for specifying datatypes for attribute values and element content.', 'Recommended'),
(26, 26, 0, 10, 'EGIF.DEI.016', 'Data exchange interoperability', '', 'ISO 3166 Code Lists - 2-letter and 3-letter country code representation standard.', 'Recommended'),
(27, 27, 0, 10, 'EGIF.DEI.017', 'Data exchange interoperability', '', 'ISO 8601 - Date and time representation standard.', 'Recommended'),
(28, 28, 0, 10, 'EGIF.DEI.018', 'Data exchange interoperability', '', 'WCO Data Model Version 3.0 ', 'Requires discussion'),
(29, 29, 1, 0, '', 'Services', '', '', ''),
(30, 30, 0, 29, 'EGIF.SRV.001', 'Services', '', 'Use of SOAP v1.1/1.2 for web service invocation and communication', 'Recommended'),
(31, 31, 0, 29, 'EGIF.SRV.002', 'Services', '', 'Description of all web services using WSDL V2.0. The web services description language describes web services in a way that other systems can consume the services', 'Mandatory'),
(32, 32, 0, 29, 'EGIF.SRV.003', 'Services', '', 'WS-I Basic Profile 1.1 or Web Services interoperability profile is a set of non-proprietary web services specifications along with clarifications and amendments to those specifications that promote interoperability.', 'Mandatory'),
(33, 33, 0, 29, 'EGIF.SRV.004', 'Services', '', 'WS-I simple SOAP binding profile v1.0 defines the use of XML envelopes for transmitting messages and places constraint on their use.', 'Mandatory'),
(34, 34, 0, 29, 'EGIF.SRV.005', 'Services', '', 'WS-I Attachments Profile 1.0 defines MIME multipart / related structure for packaging attachments with SOAP messages.', 'Recommended'),
(35, 35, 0, 29, 'EGIF.SRV.006', 'Services', '', 'Registration of all web services using Universal Description, Discovery and Integration (UDDI v3) registry.', 'Recommended'),
(36, 36, 0, 29, 'EGIF.SRV.007', 'Services', '', 'Use of hypertext transfer protocol (HTTP v1.1) and HTTPS as the application level communications protocol for web services. ', 'Recommended'),
(37, 37, 0, 29, 'EGIF.SRV.008', 'Services', '', 'Use of LDAP v3-compliant directory for authentication, authorization, and storage of identity profiles and ID management information', 'Recommended'),
(38, 38, 0, 29, 'EGIF.SRV.009', 'Services', '', 'Use of ebXML Message Service Specifications v2.0, ebXML Registry Information Model v3.0 and ebXML Registry Services Specifications v3.0 as an addition to UDDI registry. ', 'Recommended'),
(39, 39, 0, 29, 'EGIF.SRV.010', 'Services', '', 'Use of SSL v3.0 for encryption', 'Recommended'),
(40, 40, 0, 29, 'EGIF.SRV.011', 'Services', '', 'Use of integration adaptors across organizations', 'Recommended'),
(41, 41, 0, 29, 'EGIF.SRV.012', 'Services', '', 'Selection of adaptors that are certified by the application or middleware solution', 'Recommended'),
(42, 42, 0, 29, 'EGIF.SRV.013', 'Services', '', 'Domain Name Service (DNS) is a service for mapping between domain names and IP addresses', 'Mandatory'),
(43, 43, 0, 29, 'EGIF.SRV.014', 'Services', '', 'Dublin Core Standard is an extensible metadata element set intended to facilitate discovery of electronic resources.', 'Recommended'),
(44, 44, 0, 29, 'EGIF.SRV.015', 'Services', '', 'OAI harvesting protocol version 2 from Open Archives Initiative supports access to web-accessible material through interoperable repositories for metadata sharing, publishing and archiving.', 'Under review'),
(45, 45, 0, 29, 'EGIF.SRV.016', 'Services', '', 'RDF - Resource Description Framework is a method for specifying syntax of metadata used to exchange meta data by W3C', 'Under review'),
(46, 46, 0, 29, 'EGIF.SRV.017', 'Services', '', 'ODRLv2.0 - Open Digital Rights Language supports use of digital assets in the publishing, distribution and consumption of content, applications and services ', 'Under review'),
(47, 47, 0, 29, 'EGIF.SRV.018', 'Services', '', 'XrML v2.0 or eXtensible rights Markup Language is XML-based language for digital rights management (DRM)', 'Under review'),
(48, 48, 0, 29, 'EGIF.SRV.019', 'Services', '', 'OpenGIS Web Map Service Interface Standard (WMS) for GIS systems (http://www.opengeospatial.org/standards/wms)\r\n', 'Under review'),
(49, 49, 1, 0, '', 'Security', '', '', ''),
(50, 50, 0, 49, 'EGIF.SEC.001', 'Security', '', 'WS-Security to ensure security of messages transmitted between web services components ', 'Mandatory'),
(51, 51, 0, 49, 'EGIF.SEC.002', 'Security', '', 'WS-I Basic Security Profile Version 1.0 to ensure security of messages transmitted between web services', 'Recommended'),
(52, 52, 0, 49, 'EGIF.SEC.003', 'Security', '', 'X.509 international standard for digital signature certificates', 'Mandatory'),
(53, 53, 0, 49, 'EGIF.SEC.004', 'Security', '', 'SAML v1.1 - Security Assertions Markup Language (SAML) is a XML-based framework for web services that enable exchange of authentication and authorization information.', 'Recommended'),
(54, 54, 0, 49, 'EGIF.SEC.005', 'Security', '', 'S/MIME ESS Version 3 is a standard that extends the MIME specifications to support signing and encryption of email transmitted across internet', 'Recommended'),
(55, 55, 0, 49, 'EGIF.SEC.006', 'Security', '', 'XML-DSIG is a XML compliant syntax used for representing the signature of web resources and procedures for computing and verifying such signatures', 'Recommended'),
(56, 56, 1, 0, '', 'Technology', '', '', ''),
(57, 57, 0, 56, 'EGIF.TECH.001', 'Technology', '', 'Within the GoB, use intrinsic security provided by Bangladesh Computer Council Intranet (Info Sarkar and Bangla Government Networks) should be considered for all Government offices.', 'Mandatory'),
(58, 58, 0, 56, 'EGIF.TECH.002', 'Technology', '', 'National Data Center should be considered for hosting of Government data. Exceptions for establishing DC / DR for independent entities will be made on case-to-case basis.', 'Mandatory'),
(59, 59, 0, 56, 'EGIF.TECH.003', 'Technology', '', 'All entities should adhere to BD-CIRT guidelines ', 'Mandatory'),
(60, 60, 0, 56, 'EGIF.TECH.004', 'Technology', '', 'For inter-ministry system related information exchange, it is recommended to use NEA Bus for secured transfer.', 'Mandatory'),
(61, 61, 0, 56, 'EGIF.TECH.005', 'Technology', '', 'For all Government transactions requiring citizen online identity verification, NEA bus based authentication services should be used.', 'Mandatory');

-- --------------------------------------------------------

--
-- Table structure for table `dtree_mobileservice_standards`
--

CREATE TABLE `dtree_mobileservice_standards` (
  `id` bigint(20) NOT NULL,
  `node_id` bigint(20) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `parent_node` int(11) NOT NULL,
  `standard_id` varchar(100) NOT NULL,
  `node_name` text NOT NULL,
  `component` varchar(1000) NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `clasification` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dtree_mobileservice_standards`
--

INSERT INTO `dtree_mobileservice_standards` (`id`, `node_id`, `parent_id`, `parent_node`, `standard_id`, `node_name`, `component`, `description`, `clasification`) VALUES
(1, 0, 0, 0, '', 'Application Development Technologies for Mobile Devices', '', '', ''),
(2, 2, 1, 0, '', 'Windows Phone', '', '', ''),
(3, 3, 0, 2, 'Microsoft .NET Compact Framework', 'Windows Phone', 'Technology', 'The Microsoft .NET Compact Framework (.NET CF) is a version of the .NET Framework that is designed to run on Windows CE based mobile/embedded devices such as PDAs, mobile phones, factory controllers, set-top boxes, etc. The .NET Compact Framework uses some of the same class libraries as the full .NET Framework and also a few libraries designed specifically for mobile devices such as Windows CE InputPanel. ', 'Microsoft .NET Framework http://msdn.microsoft.com/en-us/netframework/aa497273.aspx \r\n'),
(4, 4, 0, 2, 'Microsoft Visual Studio IDE', 'Windows Phone', 'Tool', 'Microsoft Visual Studio is an integrated Development Environment (IDE) created by Microsoft and is used to develop computer programs for Microsoft Windows and web sites, web applications and web services. ', 'https://www.visualstudio.com/\r\n\r\n'),
(5, 5, 0, 2, 'Visual C#, Visual Basic, or Visual C++', 'Windows Phone', 'Language', 'Visual C# is an implementation of the C# language by Microsoft. Visual Studio supports Visual C# with a full-featured code editor, compiler, project templates, designers, code wizards, a powerful and easy-to-use debugger, and other tools. The .NET Framework class library provides access to many operating system services and other useful, well-designed classes that speed up the development cycle significantly.', 'https://msdn.microsoft.com/en-us/vstudio/hh341490.aspx\r\n\r\n'),
(6, 6, 1, 0, '', 'Android', '', '', ''),
(7, 7, 0, 6, 'Android Software Development ToolKit (SDK)', 'Android', 'Technology / Framework', 'The android SDK includes a comprehensive list of development tools including a debugger, libraries, emulator, documentation, tutorial and sample code.  Android studio is the official IDE, however the framework allows developer to user other IDEs (IntelliJ IDEA, NetBeans IDE).  ', 'https://en.wikipedia.org/wiki/Android_Studio'),
(8, 8, 0, 6, 'Native Development ToolKit (NDK)', 'Android', 'Technology / Framework', 'The NDK may be best described as a companion tool to the SDK which allows for implementing parts of the code using native code languages such as C and C++.  It is based on command-line tools and requires invoking them manually to build, deploy and debug the apps. It is normally suggested for usage in CPU intensive applications such as game engines, signal processing and physics simulation ', 'http://developer.android.com/tools/sdk/ndk/index_main.html\r\n'),
(9, 9, 0, 6, 'Java', 'Android', 'Language', 'Java is a class-based, object-oriented computer programming language that is designed to be platform independent and secure. The Android SDK relies heavily on standard Java libraries (data structure, math, graphics, networking, etc.) ', 'http://www.java.com/en/about/\r\n\r\n'),
(10, 10, 1, 0, '', 'iOS', '', '', ''),
(11, 11, 0, 10, 'XCode', 'iOS', 'Tools (IDE)', 'Xcode is Apple\'s powerful integrated development environment for creating apps for Mac, iPhone, and iPad. Xcode includes the Instruments analysis tool, iOS Simulator, and the latest SDKs for iOS and OS X. \r\n\r\nThe Xcode interface seamlessly integrates code editing, UI design with Interface Builder, testing, and debugging, all within a single window. The embedded Apple LLVM compiler underlines coding mistakes as you type, and is even smart enough to fix the problems automatically. \r\n', 'https://developer.apple.com/xcode/'),
(12, 12, 0, 10, 'Objective-C', 'iOS', 'Language', 'Objective-C is a general-purpose, object-oriented programming language primarily used for writing software for OSX and iOS. It’s a superset of the C programming language and provides object-oriented capabilities and a dynamic runtime. Objective-C inherits the syntax, primitive types, and flow control statements of C and adds syntax for defining classes and methods. It also adds language-level support for object graph management and object literals while providing dynamic typing and binding, deferring many responsibilities until runtime. \r\nObjective-C is inherent in iOS SDK.\r\n', 'https://developer.apple.com/library/mac/documentation/Cocoa/Conceptual/ProgrammingWithObjectiveC/Introduction/Introduction.html\r\n'),
(13, 13, 0, 10, 'SWIFT', 'iOS', 'Language', 'Swift is a new programming language for writing iOS, OS X, watchOS, and tvOS apps that builds on the best of C and Objective-C. Swift adopts safe programming patterns and adds modern features to make programming easier, more flexible, and more secure. SWIFT is a multi-paradigm, compiled programming language created by Apple Inc. for iOS, OS X, watchOS and tvOS  development. Swift is intended to be more resilient to erroneous code, with a faster compiler and new Fix-it suggestions, while being faster, more expressive and easier to understand for the developer. It also sports syntax improvements providing greater control and flow over the code and allows for interoperability with Objective-C. It is built with the LLVM compiler framework included in Xcode 6 and later and uses the Objective-C runtime, allowing C, Objective-C, C++ and Swift code to run within a single program.\r\nObjective-C is in inherent in iOS SDK (XCode) and would be made available Open Source supporting iOS, OS X and Linux.\r\n', 'https://developer.apple.com/swift/\r\n\r\n'),
(14, 14, 1, 0, '', 'Cross-Platform/Hybrid ', '', '', ''),
(15, 15, 0, 14, 'HTML5', 'Cross-Platform/Hybrid ', 'Language', 'HTML5 is a markup language used for structuring and presenting content on the World Wide Web. It was finalised and published by the W3C in Oct 2015. It is designed primarily to design web pages and addresses many of the concerns faced on older versions. It also has the ability to render multimedia without requiring plug-ins and is an open standard supported by all modern browsers. ', 'http://www.w3schools.com/html/html5_intro.asp\r\n\r\n'),
(16, 16, 0, 14, 'CSS', 'Cross-Platform/Hybrid ', 'Language', 'CSS is a style sheet language used for describing the presentation of a document written in mark-up language. It helps describe how elements are rendered on screen, paper or other media. CSS in conjunction with HTML and JS is used to create web pages, web applications and user interfaces for many mobile applications', 'http://www.w3schools.com/css/\r\n\r\n'),
(17, 17, 0, 14, 'JavaScript', 'Cross-Platform/Hybrid ', 'Language', 'JavaScript is a scripting language developed by Netscape. It is easier and faster to code in scripting languages than in structured and compiled languages such as C and C++.  Additionally JavaScript code can be embedded in the HTML pages and interpreted by the browser at run-time.', 'http://www.w3schools.com/js/\r\n\r\n'),
(18, 18, 0, 14, 'Mobile application development framework', 'Cross-Platform/Hybrid ', 'Framework', 'A multiple phone web-based application framework is a software framework that is designed to support the development of phone applications that are written as embedded dynamic websites and may leverage native phone capabilities, like geo data or contact lists. There are multiple third party frameworks available such a s- Apache Cordova, Monaca, kindo ui, Sencha Touch', 'https://cordova.apache.org/\r\nhttps://www.sencha.com/products/touch/\r\n\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `dtree_nodetable`
--

CREATE TABLE `dtree_nodetable` (
  `id` bigint(20) NOT NULL,
  `name` varchar(200) NOT NULL,
  `parent_id` bigint(20) NOT NULL,
  `Architecture_name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `column1` int(11) NOT NULL,
  `column2` int(11) NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtree_nodetable`
--

INSERT INTO `dtree_nodetable` (`id`, `name`, `parent_id`, `Architecture_name`, `description`, `column1`, `column2`, `created_date`) VALUES
(1, 'Architecture', 0, 'Application Architecture', '', 0, 0, '2015-08-26'),
(2, 'Presentation Layer', 1, 'Application Architecture', '', 0, 0, '2015-08-26'),
(3, 'Business application and service layer', 1, 'Application Architecture', '', 0, 0, '2015-08-26'),
(4, 'Data and information management layer', 1, 'Application Architecture', '', 0, 0, '2015-08-26'),
(5, 'Development tools layer', 1, 'Application Architecture', '', 0, 0, '2015-08-26'),
(6, 'Infrastructure management layer', 1, 'Application Architecture', '', 0, 0, '2015-08-26'),
(7, 'Security Layer', 1, 'Application Architecture', '', 0, 0, '2015-08-26'),
(8, 'Exception handling and log management', 1, 'Application Architecture', '', 0, 0, '2015-08-26'),
(9, 'Integration layer', 1, 'Application Architecture', '', 0, 0, '2015-08-26'),
(10, 'Service Delivery Platform', 0, 'Application Architecture', '', 0, 0, '2015-08-26');

-- --------------------------------------------------------

--
-- Table structure for table `dtree_registrationdetails`
--

CREATE TABLE `dtree_registrationdetails` (
  `firstName` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `middleName` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `lastName` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `organizationName` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `departmentName` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `emailID` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `mobileNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `phoneNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `faxNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `countryName` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `cityName` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `designation` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `roleName` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `userID` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Table Stores Registration Details of website visitor';

-- --------------------------------------------------------

--
-- Table structure for table `dtree_securityarchitecture`
--

CREATE TABLE `dtree_securityarchitecture` (
  `id` bigint(20) NOT NULL,
  `node_id` bigint(20) NOT NULL,
  `Parent_id` int(11) NOT NULL,
  `Node_name` text NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `uploads` varchar(500) NOT NULL,
  `homeFlag` int(11) NOT NULL,
  `fileDetails` longtext NOT NULL,
  `standard` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Content of Business Architecture';

-- --------------------------------------------------------

--
-- Table structure for table `dtree_security_standards`
--

CREATE TABLE `dtree_security_standards` (
  `id` bigint(20) NOT NULL,
  `node_id` bigint(20) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `parent_node` int(11) NOT NULL,
  `standard_id` varchar(100) NOT NULL,
  `node_name` text NOT NULL,
  `component` varchar(1000) NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `clasification` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dtree_security_standards`
--

INSERT INTO `dtree_security_standards` (`id`, `node_id`, `parent_id`, `parent_node`, `standard_id`, `node_name`, `component`, `description`, `clasification`) VALUES
(1, 0, 0, 0, '', 'Security standards', '', '', ''),
(2, 2, 0, 1, '1', 'Security standards', 'ISO 27001', 'Information security management', ''),
(3, 3, 0, 1, '2', 'Security standards', 'ISO 20000', 'Service management system (SMS)', ''),
(4, 4, 0, 1, '3', 'Security standards', 'ISO 22301', 'Business Continuity Management', ''),
(5, 5, 0, 1, '4', 'Security standards', 'NIST SP 800-12', 'Computer security and control', ''),
(6, 6, 0, 1, '5', 'Security standards', 'NIST SP 800-14', 'Security principles ', ''),
(7, 7, 0, 1, '6', 'Security standards', 'NIST SP 800-26', 'IT Security', ''),
(8, 8, 0, 1, '7', 'Security standards', 'NIST SP 800-37', 'Guide for Applying the Risk Management Framework', ''),
(9, 9, 0, 1, '8', 'Security standards', 'NIST SP 800-53 rev4', 'Security and Privacy Controls', ''),
(10, 10, 0, 1, '9', 'Security standards', 'PCI DSS', 'Payment Card Industry Data Security Standard for management of credit cards ', ''),
(11, 11, 0, 1, '10', 'Security standards', 'COBIT', 'Control Objectives for Information and related Technology (COBIT) - information security framework', ''),
(12, 12, 0, 1, '11', 'Security standards', 'SABSA', 'Enterprise security architecture framework', ''),
(13, 13, 0, 1, '12', 'Security standards', 'SOX', 'Sarbanes-Oxley Act of 2002 (SOX) act is also known as the public company accounting reform and investor protection act. SOX requirements indirectly compel management to consider information security controls on systems across the organization in order to comply with SOX.', ''),
(14, 14, 0, 1, '13', 'Security standards', 'ITIL - Security management', 'based on ISO 17799-is of particular relevance to the application of the information security principles', '');

-- --------------------------------------------------------

--
-- Table structure for table `dtree_standard`
--

CREATE TABLE `dtree_standard` (
  `id` int(11) NOT NULL,
  `standard` varchar(500) NOT NULL,
  `standard_table` varchar(500) NOT NULL,
  `standard_icon` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dtree_standard`
--

INSERT INTO `dtree_standard` (`id`, `standard`, `standard_table`, `standard_icon`) VALUES
(1, 'Application architecture Standards', 'dtree_applicationarchitecture_standards', 'fa-clipboard'),
(2, 'Business architecture Standards', 'dtree_businessarchitecture_standards', 'fa-briefcase'),
(3, 'Data architecture Standards', 'dtree_dataarchitecture_standards', 'fa-file-o'),
(4, 'e-GIF Standards', 'dtree_egif_standards', 'fa-star-o'),
(5, 'Mobile Service Delivery Platform Standards', 'dtree_mobileservice_standards', 'fa-mobile'),
(6, 'Security Standards', 'dtree_security_standards', 'fa-lock'),
(7, 'Technology architecture Standards', 'dtree_technologyarchitecture_standards', 'fa-laptop');

-- --------------------------------------------------------

--
-- Table structure for table `dtree_technologyarchitecture_standards`
--

CREATE TABLE `dtree_technologyarchitecture_standards` (
  `id` bigint(20) NOT NULL,
  `node_id` bigint(20) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `parent_node` int(11) NOT NULL,
  `standard_id` varchar(100) NOT NULL,
  `node_name` text NOT NULL,
  `component` varchar(1000) NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `clasification` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dtree_technologyarchitecture_standards`
--

INSERT INTO `dtree_technologyarchitecture_standards` (`id`, `node_id`, `parent_id`, `parent_node`, `standard_id`, `node_name`, `component`, `description`, `clasification`) VALUES
(1, 0, 0, 0, '', 'Technology architecture standards', '', '', ''),
(2, 2, 1, 0, '', 'Service management', '', '', ''),
(3, 3, 0, 2, 'TRM.SRV.001', 'Service management', 'Internet and Intranet Access\r\nBrowser/Mobile-Browser\r\n', 'Use Hypertext Transfer Protocol (HTTP)\r\nor Secured Hypertext Transfer Protocol (HTTPS) for access over Internet/ Intranet.\r\n', 'Mandatory'),
(4, 4, 0, 2, 'TRM.SRV.002', 'Service management', 'Internet and Intranet Access\r\nBrowser/Mobile-Browser\r\n', 'Use Hypertext Markup Language (HTML).', 'Mandatory'),
(5, 5, 0, 2, 'TRM.SRV.003', 'Service management', 'Internet and Intranet Access\r\nBrowser/Mobile-Browser\r\n', 'Use Extensible Hypertext Markup language (XHTML) as the markup language for creating web applications wherever possible. <br/>\r\n\r\nXHTML is a family of XML markup languages that mirror or extend versions of the existing widely used Hypertext Markup Language\r\n(HTML). The only essential difference between XHTML and HTML is that XHTML must be well formed XML while HTML does not impose strict XML compliance.\r\n', 'Recommended'),
(6, 6, 0, 2, 'TRM.SRV.004', 'Service management', 'Internet and Intranet Access Electronic Mail (Email)', 'Use Simple Mail Transfer Protocol (SMTP) as the standard protocol used for mail exchange amongst clients and servers.\r\nBCC has established the email systems for Government of Bangladesh officers and it is essential for all Government officers to leverage the infrastructure instead of using private email service providers considering information security.\r\n', 'Mandatory'),
(7, 7, 0, 2, 'TRM.SRV.005', 'Service management', 'Internet and Intranet AccessAccess Protocols', 'Use Hypertext Transfer Protocol Secure\r\n(HTTPS) for transactions that need to be secured over the Internet.\r\n\r\nAvoid use of transactional e-services unless these e-services are authenticated and encrypted.<br/>\r\n\r\nhttp://w3.org/TR/xhtm\r\n', 'Mandatory'),
(8, 8, 0, 2, 'TRM.SRV.006', 'Service management', 'Internet and Intranet Access\r\nAccess Protocols\r\n', 'Use Wireless Access Protocol (WAP) as the mobile Internet technology which allows mobile phone access to Internet sites.\r\n\r\nWAP is an open international standard for application layer network communications in a wireless communication environment.\r\n\r\nIts main use is to enable access to Mobile Web from a mobile phone or PDA.\r\n', 'Recommended '),
(9, 9, 0, 2, 'TRM.SRV.007', 'Service management', 'Internet and Intranet Access\r\nAccess Protocols\r\n', 'Use Wireless Transport Layer Security\r\n(WTLS) for micro browsers.\r\n', 'Recommended '),
(10, 10, 0, 2, 'TRM.SRV.008', 'Service management', 'Telephony\r\nShort Message Service (SMS)\r\n', 'There is no technical standard for compliance.', 'N.A. '),
(11, 11, 0, 2, 'TRM.SRV.009', 'Service management', 'Telephony\r\nInteractive Voice Response (IVR)\r\n', 'There is no technical standard for compliance.', 'N.A. '),
(12, 12, 0, 2, 'TRM.SRV.010', 'Service management', 'Telephony\r\nFacsimile (Fax)\r\n', 'There is no technical standard for compliance.', 'N.A. '),
(13, 13, 0, 2, 'TRM.SRV.011', 'Service management', 'Internet and Intranet Access\r\nBrowser/Mobile-Browser\r\n', 'Support latest versions of widely adopted browser(s) including<br/>\nInternet Explorer (IE) - version 6<br/>\nChrome<br/>\nFireFox<br/>\nSafari<br/>\nOpera etc.\n', 'Mandatory'),
(14, 14, 0, 2, 'TRM.SRV.012', 'Service management', 'Internet and Intranet Access\r\nBrowser/Mobile-Browser\r\n', 'The browser shall support security controls such as download Active Controls, Java permissions, cache deletion, disable cookies, HTTPS and SSL.', 'Recommended '),
(15, 15, 0, 2, 'TRM.SRV.013', 'Service management', 'Internet and Intranet Access\r\nBrowser/Mobile-Browser\r\n', 'Provide multiple modes of accessing government services (e.g. kiosks and mobile phone).', 'Recommended '),
(16, 16, 0, 2, 'TRM.SRV.014', 'Service management', 'Telephony\r\nInteractive Voice Response (IVR)\r\n', 'Implement IVR system as an alternative\r\nto Browser for access to government services.\r\n', 'Recommended '),
(17, 17, 1, 0, '', 'Platforms', '', '', ' '),
(18, 18, 0, 17, 'TRM.PLA.001', 'Platforms', 'Servers\r\nProcessor, Operating System (OS), Random Access Memory (RAM), Hard Disk (HDD), Load Balancer\r\n', 'There is no technical standard for compliance.\r\nUse rack-optimised server for efficient space management.\r\n', 'N.A. '),
(19, 19, 0, 17, 'TRM.PLA.002', 'Platforms', 'Servers\r\nProcessor, Operating System (OS), Random Access Memory (RAM), Hard Disk (HDD), Load Balancer\r\n', 'Use High-end servers to support critical business operations.\r\n\r\nUse Low-end servers for simple non-critical business operations.\r\n', 'Recommended '),
(20, 20, 0, 17, 'TRM.PLA.003', 'Platforms', 'Servers\r\nOperating System (OS)\r\n', 'Support virtualisation technologies and allow multiple operating system instances concurrently on a single physical server.', 'Recommended '),
(21, 21, 0, 17, 'TRM.PLA.004', 'Platforms', 'Clients\r\nProcessor, Operating System (OS), Random Access Memory (RAM), Hard Disk (HDD), Load Balancer\r\n', 'There is no technical standard for compliance.', 'N.A. '),
(22, 22, 0, 17, 'TRM.PLA.005', 'Platforms', 'Clients\r\nProcessor, Operating System (OS), Random Access Memory (RAM), Hard Disk (HDD)', 'Use portable computers where possible to enhance mobility and productivity.', 'Recommended'),
(23, 23, 0, 17, 'TRM.PLA.006', 'Platforms', 'Clients\r\nOperating System (OS)\r\n', 'Ensure operating system is certified and designed to run under the vendor hardware platform. Please refer to the enterprise licensing agreement for client operating system established by ITA for agencies.', 'Recommended'),
(24, 24, 0, 17, 'TRM.PLA.007', 'Platforms', 'Peripherals\r\nPeripheral Devices\r\n', 'There is no technical standard for compliance.', 'N.A.'),
(25, 25, 0, 17, 'TRM.PLA.008', 'Platforms', 'Storage and Backup\r\nStorage Area Network (SAN)/\r\nNetworked Attached Storage\r\n(NAS)\r\n', 'Support fibre channel for concurrent communication among workstations, servers and other peripherals for Storage Area Network (SAN) and Direct Attached Storage (DAS).', 'Recommended'),
(26, 26, 0, 17, 'TRM.PLA.009', 'Platforms', 'Storage and Backup\r\nNetworked Attached Storage\r\n(NAS)\r\n', 'Support Ethernet (IEEE 802.3) for NAS.', 'Recommended'),
(27, 27, 0, 17, 'TRM.PLA.010', 'Platforms', 'Storage and Backup\r\nNetworked Attached Storage\r\n(NAS)\r\n', 'Support Common Internet File System\r\n(CIFS) for file sharing for NAS.\r\n', 'Recommended'),
(28, 28, 0, 17, 'TRM.PLA.011', 'Platforms', 'Storage and Backup\r\nNetworked Attached Storage\r\n(NAS)\r\n', 'Support Network Data Management Protocol (NDMP) for controlling backup, recovery, and other transfers of data between primary and secondary storage for NAS.', 'Recommended'),
(29, 29, 0, 17, 'TRM.PLA.012', 'Platforms', 'Storage and Backup\r\nNetworked Attached Storage\r\n(NAS)\r\n', 'Support Network File System (NFS) for distributed file system for NAS.', 'Recommended'),
(30, 30, 0, 17, 'TRM.PLA.013', 'Platforms', 'Storage and Backup\r\nStorage Area Network (SAN)\r\n', 'Support Internet Small Computer System Interface (iSCSI) to provide block-level access to remote devices for SAN.', 'Recommended'),
(31, 31, 0, 17, 'TRM.PLA.014', 'Platforms', 'Storage and Backup\r\nStorage Area Network (SAN)\r\n', 'Support Fibre Channel over TCP/IP (FCIP) for connecting remote FC SANs.', 'Recommended'),
(32, 32, 0, 17, 'TRM.PLA.015', 'Platforms', 'Storage and Backup\r\nBackup System\r\n', 'There is no technical standard for compliance. Please refer to Architecture\r\nDesign Considerations or Best Practices for more information.\r\n', 'N.A.'),
(33, 33, 0, 17, 'TRM.PLA.016', 'Platforms', 'Platform Management and\r\nSecurity\r\nServer Management/ Client Management\r\n', 'Support Directory Enabled Networking\r\n(DEN) to map service and policy to directory.\r\n', 'Recommended'),
(34, 34, 0, 17, 'TRM.PLA.017', 'Platforms', 'Platform Management and\r\nSecurity\r\nClient Management\r\n', 'Support Desktop Management Interface\r\n(DMI) standards to collect information about a computer environment for desktop management.\r\n', 'Recommended'),
(35, 35, 0, 17, 'TRM.PLA.018', 'Platforms', 'Platform Management and\r\nSecurity\r\nServer Management\r\n', 'Support Web-Based Enterprise\r\nManagement (WBEM) to enable server management through web-enabled application.\r\n', 'Recommended'),
(36, 36, 0, 17, 'TRM.PLA.019', 'Platforms', 'Platform Management and\r\nSecurity\r\nServer Management\r\n', 'Support Alert Standard Format (ASF) to define OS-absent alerting for preventive monitoring.', 'Recommended'),
(37, 37, 0, 17, 'TRM.PLA.020', 'Platforms', 'Platform Management and\r\nSecurity\r\nPlatform Security\r\n', 'Support hardened operating system.', 'Recommended'),
(38, 38, 0, 17, 'TRM.PLA.021', 'Platforms', 'Platform Management and\r\nSecurity\r\nPlatform Security\r\n', 'Support Trusted Platform Module (TPM) for authenticating mobile computing device.', 'Recommended'),
(39, 39, 0, 17, 'TRM.PLA.022', 'Platforms', 'Storage and Backup\r\nStorage Area Network (SAN)\r\n', 'Use SAN for enterprise storage solution.\r\nPlease refer to Paragraph 4.6.4(a) for SAN solution guidance.\r\n', 'Recommended'),
(40, 40, 0, 17, 'TRM.PLA.023', 'Platforms', 'Storage and Backup\r\nBackup System\r\n', 'Implement enterprise-wide backup solution. Please refer to Paragraph 4.6.4(a) for backup solution guidance.', 'Recommended'),
(41, 41, 1, 0, '', 'Networks', '', '', ''),
(42, 42, 0, 41, 'TRM.NW.001', 'Networks', 'WAN, LAN, WLAN\r\nAll technology components\r\n', 'Use TCP/IP as standard network protocol for all government agencies.', 'Mandatory'),
(43, 43, 0, 41, 'TRM.NW.002', 'Networks', 'WAN, LAN, WLAN\r\nAll technology components\r\n', 'All devices in LAN and WAN infrastructure shall support IPv6 standards (128 bits for addressing).', 'Recommended'),
(44, 44, 0, 41, 'TRM.NW.003', 'Networks', 'WAN\r\nNetwork Communication Devices\r\n', 'Support Open Shortest Path First (OSPF, OSPF2, Multi-path OSPF) for core switch.', 'Recommended'),
(45, 45, 0, 41, 'TRM.NW.004', 'Networks', 'WAN\r\nNetwork Communication Devices/ Network Security Devices\r\n', 'Support Internet Protocol Security (IPSec) for secure exchange packets at IP layer and IKE (Internet Key Exchange) for key exchange.', 'Recommended'),
(46, 46, 0, 41, 'TRM.NW.005', 'Networks', 'WAN\r\nNetwork Communication Devices/ Network Security Devices\r\n', 'Support Secure Sockets Layer (SSLv3) for mutual authentication between a client and server.', 'Recommended'),
(47, 47, 0, 41, 'TRM.NW.006', 'Networks', 'WAN\r\nNetwork Communication Devices/ Network Security Devices\r\n', 'Support SSH for secure remote login, secure file transfer and secure TCP/IP and X11 forwarding.', 'Recommended'),
(48, 48, 0, 41, 'TRM.NW.007', 'Networks', 'WAN\r\nNetwork Communication Devices/ Network Security Devices\r\n', 'Support IEEE 802.11i to enhance 802.11\r\nMedium Access Control (MAC) for higher security and authentication mechanisms.\r\n', 'Recommended'),
(49, 49, 0, 41, 'TRM.NW.008', 'Networks', 'WAN\r\nNetwork Security Devices\r\n', 'Certified to Common Criteria EAL-4\r\n(Evaluation Assurance Level) for firewall.\r\n', 'Recommended'),
(50, 50, 0, 41, 'TRM.NW.009', 'Networks', 'WAN\r\nNetwork Security Devices\r\n', 'Authenticate using two factor authentication methods such as Token or One-time Password (RFC 2289).', 'Recommended'),
(51, 51, 0, 41, 'TRM.NW.010', 'Networks', 'WAN\r\nTransport Method\r\n', 'Support Multi-Protocol Label Switching\r\n(MPLS).\r\n', 'Mandatory'),
(52, 52, 0, 41, 'TRM.NW.011', 'Networks', 'WAN\r\nTransport Method\r\n', 'Support H.320 for audio, video and graphical communications.', 'Recommended '),
(53, 53, 0, 41, 'TRM.NW.012', 'Networks', 'LAN\r\nNetwork Communication\r\nDevices / Network Interface Card (NIC)\r\n', 'Support any of the following:<br/>\r\n(a) IEEE 802.3u-100Base T (for Fast<br/>\r\nEthernet over twisted pair cables)<br/>\r\n(b) IEEE 802.3u-100BaseFx (for fast\r\nEthernet over optical fibre)<br/>\r\n(c) IEEE 802.3ab (1 Gbps over Cat5e/6\r\ncabling system)<br/>\r\n(d) IEEE 802.3z (for Gigabit Ethernet over fibre and cable).\r\n', 'Mandatory '),
(54, 54, 0, 41, 'TRM.NW.013', 'Networks', 'LAN\r\nNetwork Communication\r\nDevices \r\n', 'Support Dynamic Host Configuration\r\nProtocol (DHCP) for dynamic IP addresses assignment to devices.\r\n', 'Mandatory '),
(55, 55, 0, 41, 'TRM.NW.014', 'Networks', 'LAN\r\nNetwork Communication\r\nDevices \r\n', 'Support IEEE 802.1w (Rapid Spanning Tree Protocol) to provide rapid reconfiguration capability.', 'Recommended '),
(56, 56, 0, 41, 'TRM.NW.015', 'Networks', 'LAN\r\nNetwork Communication\r\nDevices \r\n', 'Support IEEE 802.3ad for link aggregation for edge switch.', 'Recommended '),
(57, 57, 0, 41, 'TRM.NW.016', 'Networks', 'LAN\r\nNetwork Communication\r\nDevices \r\n', 'Support IEEE 802.3x to define full duplex\r\noperation and flow control on 100Mbps\r\nEthernet network for edge switch.\r\n', 'Recommended '),
(58, 58, 0, 41, 'TRM.NW.017', 'Networks', 'LAN\r\nNetwork Communication\r\nDevices \r\n', 'Support Virtual Router Redundancy\r\nProtocol (VRRP) to eliminate the single\r\npoint of failure inherent in the static default routed environment for core switch.\r\n', 'Recommended '),
(59, 59, 0, 41, 'TRM.NW.018', 'Networks', 'LAN\r\nNetwork Communication\r\nDevices \r\n', 'Support Differentiated Service (DiffServ) to provide QoS to the traffic for core switch.', 'Recommended '),
(60, 60, 0, 41, 'TRM.NW.019', 'Networks', 'LAN\r\nNetwork Communication\r\nDevices \r\n', 'Support IEEE 802.1q for Virtual LAN\r\n(VLAN).\r\n', 'Recommended '),
(61, 61, 0, 41, 'TRM.NW.020', 'Networks', 'LAN\r\nNetwork Communication\r\nDevices \r\n', 'Support 1000Base-LH (Long Haul) to provide gigabit speed over distance between 70 and 100km.', 'Recommended '),
(62, 62, 0, 41, 'TRM.NW.021', 'Networks', 'LAN\r\nNetwork Communication\r\nDevices \r\n', 'Support IEEE802.3af for edge switches supporting devices which require twisted pair cables (e.g. IP Phone Clients and wireless LAN access points).', 'Recommended '),
(63, 63, 0, 41, 'TRM.NW.022', 'Networks', 'LAN\r\nNetwork Communication\r\nDevices \r\n', 'Support IEEE 802.3ae to support operating speed of 10Gbps Ethernet over fibre for core switch.', 'Recommended '),
(64, 64, 0, 41, 'TRM.NW.023', 'Networks', 'LAN\r\nStructured Cabling System\r\n', 'Use Unshielded Twisted Pair (UTP) Category 6 for Structured Cabling System based on ANSI/TIA/EIA-568-B.2-1.', 'Recommended '),
(65, 65, 0, 41, 'TRM.NW.024', 'Networks', 'LAN\r\nStructured Cabling System\r\n', 'Use fibre cables to interconnect network devices and backbone connections for Structured Cabling system as described by TIA/EIA 568. Multimode fibre is used for short distance transmissions with LED based fibre optic equipment. Single-mode fibre is used for long distance transmissions with laser diode based fibre optic transmission equipment.<br/>\r\nPhysical layer standards for optical fibre are:<br/>\r\n(a) Support 1000Base-SX (short\r\nwavelength laser) to provide gigabit\r\nspeed over maximum distance of 220m\r\n(for 62.5 micron multimode fibre)\r\nand 550m (for 50 micron multimode\r\nfibre).<br/>\r\n(b) Support 1000Base-LX (long wavelength laser) to provide gigabit speed over maximum distance of 550m (for 50 and 62.5 micron multimode fibre). upto five km single mode with 9 micron fibre\r\n', 'Recommended '),
(66, 66, 0, 41, 'TRM.NW.025', 'Networks', 'LAN\r\nStructured Cabling System\r\n', 'Use Commercial Building\r\nTelecommunications Cabling Standard\r\n2001 based on ANSI/TIA/EIA 568-B.\r\n', 'Recommended '),
(67, 67, 0, 41, 'TRM.NW.026', 'Networks', 'LAN\r\nStructured Cabling System\r\n', 'Use Generic Cabling for Customer Premises (International Standards) 2002 based on ISO/IEC 11801.', 'Recommended '),
(68, 68, 0, 41, 'TRM.NW.027', 'Networks', 'LAN\r\nStructured Cabling System\r\n', 'Use Generic Cabling Systems (CENELEC\r\nStandards) 2002 based on EN 50173.\r\n', 'Recommended '),
(69, 69, 0, 41, 'TRM.NW.028', 'Networks', 'LAN\r\nStructured Cabling System\r\n', 'Use Generic Universal Cabling Infrastructure\r\nwith support voice and data applications\r\nbased on ISO/IEC 11801, ISO/IEC 11801,\r\n14763-1,\r\n14763-2,\r\n14763-3,\r\nIEC 61935-1,\r\nTIA/EIA 568-B,\r\nEN50173,\r\nTIA/EIA 606-A,\r\nIEC332-1\r\n', 'Recommended '),
(70, 70, 0, 41, 'TRM.NW.029', 'Networks', 'LAN\r\nStructured Cabling System\r\n', 'Use Commercial Building Standard for\r\nTelecommunications Pathways and Spaces 2004.\r\n', 'Recommended '),
(71, 71, 0, 41, 'TRM.NW.030', 'Networks', 'LAN\r\nStructured Cabling System\r\n', 'Build and install cables based on ISO/IEC\r\n18010 standards of Information Technology\r\n- Pathways and Spaces for Customer\r\nPremises Cabling.\r\n', 'Recommended '),
(72, 72, 0, 41, 'TRM.NW.031', 'Networks', 'LAN\r\nStructured Cabling System\r\n', 'Test cables after installation based on TIA/ EIA-568-B and IEC 61935 standards.', 'Recommended '),
(73, 73, 0, 41, 'TRM.NW.032', 'Networks', 'LAN\r\nFree Space Optics (FSO)\r\n', 'Support Class 1 or Class 3 (excluding Class 3B) laser for FSO.', 'Recommended '),
(74, 74, 0, 41, 'TRM.NW.033', 'Networks', 'WLAN\r\nAll technology components\r\n', 'Implement WLAN that supports any of the following standards:<br/>\r\n\r\n(a) Wi-Fi Protected Access (WPA)<br/>\r\n(b) WPA2<br/>\r\n(c) Advanced Encryption Standard (AES)<br/>\r\n(d) Mobile Virtual Private Networks (VPNs).\r\n', 'Mandatory '),
(75, 75, 0, 41, 'TRM.NW.034', 'Networks', 'WLAN\r\nWireless Access Point (AP)/\r\nAccess Controller\r\n', 'Support IEEE 802.11a for 54 Mbps high speed wireless LAN and 5 GHz range.', 'Recommended '),
(76, 76, 0, 41, 'TRM.NW.035', 'Networks', 'WLAN\r\nWireless Access Point (AP)/\r\nAccess Controller\r\n', 'Support IEEE 802.11g for 54 Mbps high speed wireless LAN and 2.4 GHz range.', 'Recommended '),
(77, 77, 0, 41, 'TRM.NW.036', 'Networks', 'WLAN\r\nWireless Access Point (AP)/\r\nAccess Controller\r\n', 'Support IEEE 802.11n for 54 Mbps high\r\nspeed wireless LAN up to 600 Mbps (with\r\n2.4 GHz and 5 GHz range).\r\n', 'Recommended '),
(78, 78, 0, 41, 'TRM.NW.037', 'Networks', 'IP Telephony and Video\r\nConferencing\r\nIP-Telephony Gateway\r\n', 'Support H.323 for converting between voice and data transmission formats and for managing connections between telephony endpoint and Real-Time Transport Protocol (RTP).', 'Recommended '),
(79, 79, 0, 41, 'TRM.NW.038', 'Networks', 'IP Telephony and Video\r\nConferencing\r\nIP-Telephony Gateway\r\n', 'Support H.248 for controlling media gateways on Internet Protocol (IP) network and Public Switched Telephone Network (PSTN).', 'Recommended '),
(80, 80, 0, 41, 'TRM.NW.039', 'Networks', 'IP Telephony and Video\r\nConferencing\r\nIP-Telephony Gateway\r\n', 'Support RTP for end-to-end network\r\ntransmission of real-time data, such as\r\naudio, video or simulation data, over multicast or unicast network services.\r\n', 'Recommended '),
(81, 81, 0, 41, 'TRM.NW.040', 'Networks', 'IP Telephony and Video\r\nConferencing\r\nIP-Telephony Gateway\r\n', 'Support Real Time Streaming Protocol\r\n(RTSP) for control over the delivery of data with real-time properties.\r\n', 'Recommended '),
(82, 82, 0, 41, 'TRM.NW.041', 'Networks', 'IP Telephony and Video\r\nConferencing\r\nIP-Telephony Gateway\r\n', 'Support H.263 for compression algorithm and optimization for lower data rates.', 'Recommended '),
(83, 83, 0, 41, 'TRM.NW.042', 'Networks', 'IP Telephony and Video\r\nConferencing\r\nIP-Telephony Gateway/ IP Phone Client\r\n', 'Use Session Initiation Protocol (SIP) to manage IP telephony sessions.\r\nSIP is an application-layer control (signalling) protocol for creating, modifying, and terminating sessions with one or more participants. These sessions include Internet telephone calls, multimedia distribution, and multimedia conferences.\r\n', 'Recommended '),
(84, 84, 0, 41, 'TRM.NW.043', 'Networks', 'Network Management\r\nFault Management /\r\nPerformance Monitoring and\r\nManagement\r\n', 'Use Simple Network Management\r\nProtocol (SNMP) v2 and above as the main management protocol suite.\r\n', 'Recommended '),
(85, 85, 0, 41, 'TRM.NW.044', 'Networks', 'IP Telephony and Video\r\nConferencing\r\nIP-Telephony Gateway\r\n', 'Use IP Telephony where possible.', 'Recommended '),
(86, 86, 0, 41, 'TRM.NW.045', 'Networks', 'IP Telephony and Video\r\nConferencing\r\nIP-Telephony Gateway\r\n', 'Use video conferencing system for collaboration where possible.', 'Recommended '),
(87, 87, 0, 41, 'TRM.NW.046', 'Networks', 'Network Management\r\nFault Management /\r\nPerformance Monitoring and\r\nManagement\r\n', 'Use network management tools to manage LAN.', 'Recommended '),
(88, 88, 1, 0, '', 'Data center', '', '', ''),
(89, 89, 0, 88, 'TRM.DC.001\r\n', 'Data Center', 'Physical Site Layout, Cabling Infrastructure, Tiered Reliability, Environmental Factors', 'Design data center in accordance to TIA\r\n942 standards.\r\n', 'Recommended '),
(90, 90, 0, 88, 'TRM.DC.002\r\n', 'Data Center', 'Physical Site Layout\r\nAll physical rooms and areas\r\nwithin the data center\r\n', 'Design data center with ample space for expansion to meet the growing demands. Locate the data center at a physically safe area.', 'Recommended '),
(91, 91, 0, 88, 'TRM.DC.003\r\n', 'Data Center', 'Physical Site Layout\r\nAll physical rooms and areas  within the data center\r\n', 'Implement 24/7 physical security monitoring through CCTV Surveillance Monitoring (e.g.\r\nClosed-circuit television (CCTV) /Automated Security Intrusion Alarm/Biometric/Motion Detector) with minimally an intrusion response exercise annually.\r\n', 'Recommended '),
(92, 92, 0, 88, 'TRM.DC.004\r\n', 'Data Center', 'Physical Site Layout\r\nComputer/Server Room\r\n', 'Standardize use of 19-inch 42U racks which aids better cabling management and for cold/ hot air aisle efficiency.\r\n\r\nAll racks should have perforated doors for front and back for front-in and back-out cross-air movement.\r\n', 'Mandatory '),
(93, 93, 0, 88, 'TRM.DC.005', 'Data Center', 'Physical Site Layout\r\nComputer/Server Room\r\n', 'Install man-trap access to computer\r\nroom as an additional barrier to prevent unauthorized access to the computer room.\r\n', 'Recommended '),
(94, 94, 0, 88, 'TRM.DC.006', 'Data Center', 'Physical Site Layout\r\nAll physical rooms and areas\r\nwithin the data center\r\n', 'Conduct a risk assessment before building or implementing a data center. Implement appropriate controls to mitigate identified risks.', 'Mandatory'),
(95, 95, 0, 88, 'TRM.DC.007', 'Data Center', 'Physical Site Layout\r\nAll physical rooms and areas\r\nwithin the data center\r\n', 'Separate the location of disaster recovery site from the primary data center.', 'Mandatory'),
(96, 96, 0, 88, 'TRM.DC.008', 'Data Center', 'Physical Site Layout\r\nAll physical rooms and areas\r\nwithin the data center\r\n', 'Ensure smoke detection and fire suppression systems are in place and tested on periodic basis.', 'Mandatory'),
(97, 97, 0, 88, 'TRM.DC.009', 'Data Center', 'Physical Site Layout\r\nAll physical rooms and areas\r\nwithin the data center\r\n', 'Design data center with ample space for growth.', 'Recommended'),
(98, 98, 0, 88, 'TRM.DC.010', 'Data Center', 'Physical Site Layout\r\nAll physical rooms and areas\r\nwithin the data center\r\n', 'Locate the data center at a physically safe area.', 'Recommended'),
(99, 99, 0, 88, 'TRM.DC.011', 'Data Center', 'Cabling Infrastructure\r\nBackbone Cabling\r\n', 'Use Fibre Optic Cable (FOC) for backbone cabling.', 'Recommended'),
(100, 100, 0, 88, 'TRM.DC.012', 'Data Center', 'Cabling Infrastructure\r\nHorizontal Cabling\r\n', 'Use Category 6 for horizontal cabling.', 'Recommended'),
(101, 101, 0, 88, 'TRM.DC.013', 'Data Center', 'Tiered Reliability\r\nData Centre Tiers\r\n', 'Design and operate at minimum Tier II and where possible to have Tier III data center or higher.', 'Recommended'),
(102, 102, 0, 88, 'TRM.DC.014', 'Data Center', 'Environmental Factors\r\nPower/Cooling\r\n', 'Carry out a detailed capacity requirements study for space, power and cooling.', 'Recommended'),
(103, 103, 0, 88, 'TRM.DC.015', 'Data Center', 'Environmental Factors\r\nPower/Cooling\r\n', 'Implement “hot” and “cold” aisle setup for effective cooling.', 'Recommended'),
(104, 104, 1, 0, '', 'Cloud', '\r\n', '', ''),
(105, 105, 0, 104, 'TRM.CLO.001', 'Cloud', 'Authentication and Authorization', 'RFC 5246 Secure Sockets Layer (SSL)/ Transport Layer Security (TLS)', 'Recommended'),
(106, 106, 0, 104, 'TRM.CLO.002', 'Cloud', 'Authentication and Authorization', 'RFC 3820: X.509 Public Key Infrastructure (PKI) Proxy Certificate Profile', 'Recommended'),
(107, 107, 0, 104, 'TRM.CLO.003', 'Cloud', 'Authentication and Authorization', 'RFC5280: Internet X.509 Public Key Infrastructure Certificate and\r\nCertificate Revocation List (CRL) Profile\r\n', 'Recommended'),
(108, 108, 0, 104, 'TRM.CLO.004', 'Cloud', 'Authentication and Authorization', 'RFC 5849 OAuth (Open Authorization Protocol)', 'Recommended'),
(109, 109, 0, 104, 'TRM.CLO.005', 'Cloud', 'Authentication and Authorization', 'ISO/IEC 9594-8:2008 | X.509\r\nInformation technology -- Open Systems Interconnection -- The Directory: Publickey and attribute certificate frameworks\r\n', 'Recommended'),
(110, 110, 0, 104, 'TRM.CLO.006', 'Cloud', 'Authentication and Authorization', 'ISO/IEC 29115 | X.1254\r\nInformation technology - Security techniques -- Entity authentication\r\nassurance framework\r\n', 'Recommended'),
(111, 111, 0, 104, 'TRM.CLO.007', 'Cloud', 'Authentication and Authorization', 'OpenID Authentication', 'Recommended'),
(112, 112, 0, 104, 'TRM.CLO.008', 'Cloud', 'Authentication and Authorization', 'eXtensible Access Control Markup Language (XACML)', 'Recommended'),
(113, 113, 0, 104, 'TRM.CLO.009', 'Cloud', 'Authentication and Authorization', 'Security Assertion Markup Language (SAML)', 'Recommended'),
(114, 114, 0, 104, 'TRM.CLO.010', 'Cloud', 'Confidentiality', 'RFC 5246 Secure Sockets Layer (SSL)/ Transport Layer Security (TLS)', 'Recommended'),
(115, 115, 0, 104, 'TRM.CLO.011', 'Cloud', 'Confidentiality', 'Key Management Interoperability Protocol (KMIP)', 'Recommended'),
(116, 116, 0, 104, 'TRM.CLO.012', 'Cloud', 'Confidentiality', 'XML Encryption Syntax and Processing', 'Recommended'),
(117, 117, 0, 104, 'TRM.CLO.013', 'Cloud', 'Integrity', 'XML signature (XMLDSig)', 'Recommended'),
(118, 118, 0, 104, 'TRM.CLO.014', 'Cloud', 'Identity management', 'Service Provisioning Markup Language (SPML)', 'Recommended'),
(119, 119, 0, 104, 'TRM.CLO.015', 'Cloud', 'Identity management', 'Web Services Federation Language (WSFederation) Version 1.2', 'Recommended'),
(120, 120, 0, 104, 'TRM.CLO.016', 'Cloud', 'Identity management', 'WS-Trust 1.3', 'Recommended'),
(121, 121, 0, 104, 'TRM.CLO.017', 'Cloud', 'Identity management', 'Security Assertion Markup Language (SAML)', 'Recommended'),
(122, 122, 0, 104, 'TRM.CLO.018', 'Cloud', 'Identity management', 'OpenID Authentication 1.', 'Recommended'),
(123, 123, 0, 104, 'TRM.CLO.019', 'Cloud', 'Security Monitoring and Incident Response', 'ISO/IEC WD 27035-1 Information technology -- Security techniques -- Information security incident management -- Part 1: Principles of incident management', 'Recommended'),
(124, 124, 0, 104, 'TRM.CLO.020', 'Cloud', 'Security Monitoring and Incident Response', 'ISO/IEC WD 27035-3 Information technology -- Security techniques -- Information security incident management -- Part 3: Guidelines for CSIRT operations', 'Recommended'),
(125, 125, 0, 104, 'TRM.CLO.021', 'Cloud', 'Security Monitoring and Incident Response', 'ISO/IEC WD 27039; Information technology -- Security techniques -- Selection, deployment and operations of intrusion detection systems', 'Recommended'),
(126, 126, 0, 104, 'TRM.CLO.022', 'Cloud', 'Security Monitoring and Incident Response', 'ISO/IEC 18180 Information technology - Specification for the Extensible Configuration Checklist Description Format (XCCDF) Version 1.2 (NIST IR 7275)', 'Recommended'),
(127, 127, 0, 104, 'TRM.CLO.023', 'Cloud', 'Security Monitoring and Incident Response', 'X.1500 Cybersecurity information exchange techniques', 'Recommended'),
(128, 128, 0, 104, 'TRM.CLO.024', 'Cloud', 'Security Monitoring and Incident Response', 'X.1520: Common vulnerabilities and exposures', 'Recommended'),
(129, 129, 0, 104, 'TRM.CLO.025', 'Cloud', 'Security Monitoring and Incident Response', 'X.1521 Common Vulnerability Scoring System', 'Recommended'),
(130, 130, 0, 104, 'TRM.CLO.026', 'Cloud', 'Security Monitoring and Incident Response', 'PCI Data Security Standard', 'Recommended'),
(131, 131, 0, 104, 'TRM.CLO.027', 'Cloud', 'Security Controls', 'Cloud Controls Matrix Version 1.3', 'Recommended'),
(132, 132, 0, 104, 'TRM.CLO.028', 'Cloud', 'Security Controls', 'ISO/IEC 27001:2005 Information Technology - Security Techniques Information Security Management Systems Requirements', 'Recommended'),
(133, 133, 0, 104, 'TRM.CLO.029', 'Cloud', 'Security Controls', 'ISO/IEC WD TS 27017 Information technology -- Security techniques -- Information security management - Guidelines on information security controls for the use of cloud computing services based on ISO/IEC 27002', 'Recommended'),
(134, 134, 0, 104, 'TRM.CLO.030', 'Cloud', 'Security Controls', 'ISO/IEC 27018 Code of Practice for Data Protection Controls for Public Cloud Computing Services', 'Recommended'),
(135, 135, 0, 104, 'TRM.CLO.031', 'Cloud', 'Security Controls', 'ISO/IEC 1st WD 27036-4 Information technology - Security techniques - Information security for supplier relationships - Part 4: Guidelines for security of cloud services', 'Recommended'),
(136, 136, 0, 104, 'TRM.CLO.032', 'Cloud', 'Security Policy Management', 'ISO/IEC 27002 Code of practice for information security management', 'Recommended'),
(137, 137, 0, 104, 'TRM.CLO.033', 'Cloud', 'Security Policy Management', 'eXtensible Access Control Markup Language (XACML)', 'Recommended'),
(138, 138, 0, 104, 'TRM.CLO.034', 'Cloud', 'Availability', 'ISO/PAS 22399:2007 Societal security - Guideline for incident preparedness and operational continuity management', 'Recommended'),
(139, 139, 0, 104, 'TRM.CLO.035', 'Cloud', 'Service interoperability', 'IEEE P2301, Draft Guide for Cloud Portability and Interoperability Profiles (CPIP)', 'Recommended'),
(140, 140, 0, 104, 'TRM.CLO.036', 'Cloud', 'Service interoperability', 'IEEE P2302, Draft Standard for Intercloud Interoperability and Federation (SIIF)', 'Recommended'),
(141, 141, 0, 104, 'TRM.CLO.037', 'Cloud', 'Service interoperability', 'Y.3520 Cloud computing framework for end to end resource management (ITU)', 'Recommended'),
(142, 142, 0, 104, 'TRM.CLO.038', 'Cloud', 'Service interoperability', 'OASIS Cloud Application Management Platform (CAMP) ', 'Recommended'),
(143, 143, 0, 104, 'TRM.CLO.039', 'Cloud', 'Service interoperability', 'OASIS Topology and Orchestration Specification or Cloud Applications (TOSCA),Version 1.0 Committee Specification Draft 06 / Public Review Draft 01', 'Recommended'),
(144, 144, 0, 104, 'TRM.CLO.040', 'Cloud', 'Service interoperability', 'Open Cloud Computing Interface (OCCI)', 'Recommended');

-- --------------------------------------------------------

--
-- Table structure for table `dtree_users`
--

CREATE TABLE `dtree_users` (
  `id` bigint(20) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'active',
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dtree_users`
--

INSERT INTO `dtree_users` (`id`, `username`, `password`, `status`, `created_date`) VALUES
(1, 'nea_portal', 'Portal_user#qwerty', 'active', '2015-08-12');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`) VALUES
(1, 'language', 'detect'),
(2, 'template', 'scratch'),
(3, 'title', 'NEA Portal'),
(4, 'author', 'BCC'),
(5, 'copyright', ''),
(6, 'description', 'National Enterprise Architecture'),
(7, 'keywords', ''),
(8, 'robots', 'all'),
(9, 'email', 'ramesh1.mishra@in.ey.com'),
(10, 'subject', 'NEA Portal'),
(11, 'notification', '0'),
(12, 'charset', 'utf-8'),
(13, 'divider', ' - '),
(14, 'time', 'H:i'),
(15, 'date', 'd.m.Y'),
(16, 'homepage', '1'),
(17, 'limit', '10'),
(18, 'order', 'asc'),
(19, 'pagination', '1'),
(20, 'moderation', '0'),
(21, 'registration', '1'),
(22, 'verification', '0'),
(23, 'reminder', '1'),
(24, 'captcha', '0'),
(25, 'blocker', '1'),
(26, 'version', '2.4.0');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first` datetime DEFAULT NULL,
  `last` datetime DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `groups` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `user`, `password`, `email`, `description`, `language`, `first`, `last`, `status`, `groups`) VALUES
(1, 'nea', 'admin', '88915f05a04452de794b8a775adc46c852fc5be1a29fc8caddf990177b12381770bbfd6fffabfe26', 'ramesh1.mishra@in.ey.com', 'God admin', '', NULL, '2015-06-27 13:09:14', 1, '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dtree_applicationarchitecture`
--
ALTER TABLE `dtree_applicationarchitecture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dtree_applicationarchitecture_standards`
--
ALTER TABLE `dtree_applicationarchitecture_standards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dtree_businessarchitecture`
--
ALTER TABLE `dtree_businessarchitecture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dtree_businessarchitecture_standards`
--
ALTER TABLE `dtree_businessarchitecture_standards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dtree_dataarchitecture`
--
ALTER TABLE `dtree_dataarchitecture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dtree_dataarchitecture_standards`
--
ALTER TABLE `dtree_dataarchitecture_standards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dtree_egif_standards`
--
ALTER TABLE `dtree_egif_standards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dtree_mobileservice_standards`
--
ALTER TABLE `dtree_mobileservice_standards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dtree_nodetable`
--
ALTER TABLE `dtree_nodetable`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dtree_registrationdetails`
--
ALTER TABLE `dtree_registrationdetails`
  ADD PRIMARY KEY (`userID`),
  ADD UNIQUE KEY `userID` (`userID`),
  ADD KEY `userID_2` (`userID`);

--
-- Indexes for table `dtree_securityarchitecture`
--
ALTER TABLE `dtree_securityarchitecture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dtree_security_standards`
--
ALTER TABLE `dtree_security_standards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dtree_standard`
--
ALTER TABLE `dtree_standard`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dtree_technologyarchitecture_standards`
--
ALTER TABLE `dtree_technologyarchitecture_standards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dtree_users`
--
ALTER TABLE `dtree_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dtree_applicationarchitecture`
--
ALTER TABLE `dtree_applicationarchitecture`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `dtree_applicationarchitecture_standards`
--
ALTER TABLE `dtree_applicationarchitecture_standards`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `dtree_businessarchitecture`
--
ALTER TABLE `dtree_businessarchitecture`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `dtree_businessarchitecture_standards`
--
ALTER TABLE `dtree_businessarchitecture_standards`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `dtree_dataarchitecture`
--
ALTER TABLE `dtree_dataarchitecture`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `dtree_dataarchitecture_standards`
--
ALTER TABLE `dtree_dataarchitecture_standards`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT for table `dtree_egif_standards`
--
ALTER TABLE `dtree_egif_standards`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `dtree_mobileservice_standards`
--
ALTER TABLE `dtree_mobileservice_standards`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `dtree_nodetable`
--
ALTER TABLE `dtree_nodetable`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `dtree_securityarchitecture`
--
ALTER TABLE `dtree_securityarchitecture`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dtree_security_standards`
--
ALTER TABLE `dtree_security_standards`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `dtree_standard`
--
ALTER TABLE `dtree_standard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `dtree_technologyarchitecture_standards`
--
ALTER TABLE `dtree_technologyarchitecture_standards`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=145;
--
-- AUTO_INCREMENT for table `dtree_users`
--
ALTER TABLE `dtree_users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
