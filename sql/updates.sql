CREATE TABLE `news_events` (
  `id` bigint(20) PRIMARY KEY AUTO_INCREMENT ,
  `title` varchar(500) NOT NULL,
  `publish_date` DATETIME  NOT NULL,
  `image_path` varchar(500),
  `description` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `upload_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `isPublished` int(11) default 1,
  `isDeleted` int(11) default 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE news_events AUTO_INCREMENT=100;


CREATE TABLE `key_persons_info` (
  `id` int PRIMARY KEY AUTO_INCREMENT ,
  `name` varchar(500) NOT NULL,
  `designation`  varchar(500),
  `display_order`  int,
  `isPublished` int(11) default 1,
  `image_path` varchar(500),
  `description` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `color_code` varchar(20),
  `upload_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `isDeleted` int(11) default 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE key_persons_info AUTO_INCREMENT=100;

INSERT INTO `users` (`id`, `name`, `user`, `password`, `email`, `description`, `language`, `first`, `last`, `status`, `groups`) VALUES
(2, 'nda', 'admin2', 'admin2', 'ahasanul.hadi@gmail.com', 'God admin', '', NULL, '2015-06-27 13:09:14', 1, '1');