<!--================Header Menu Area =================-->
<style>

    .bangladesh{
        color: #ea2c58;font-size: 22px;
    }
    .nea{
        color: #006a4e;
        font-size: 18px
    }
</style>

<header class="header_area">
    <div class="main_menu">
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container ">
                <!-- Brand and toggle get grouped for better mobile display -->

                <div class="col-10 col-md-10 col-md-8 col-lg-4">
                    <img  style="float: left; height: 75px;"  src="<?php echo  $GLOBALS['root'];?>/images/nea-logo.png" alt="">
                    <p style=" margin-top: 7px;" class="navbar-brand logo_h bold hidden-sm" href="index.php">
                        <span class="d-none d-sm-block bangladesh" >Bangladesh </span>
                        <span class="d-none d-sm-block nea" > National Digital Architecture </span>
                    </p>
                </div>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse offset" id="navbarSupportedContent" style="margin-top: 0px;">
                    <ul class="nav navbar-nav menu_nav ml-auto">
                        <li class="nav-item active"><a class="nav-link" href="<?php echo  $GLOBALS['root'];?>/index.php">Home</a></li>
                        <li class="nav-item"><a class="nav-link" href="http://103.48.16.82:8080/OpenKM/login.jsp">Repository</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo  $GLOBALS['root'];?>/pages/standards.php">Standards</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo  $GLOBALS['root'];?>/pages/services.php">E-Services</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo  $GLOBALS['root'];?>/pages/assessment.php">Maturity Assessment</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo  $GLOBALS['root'];?>/pages/contact.php">Contact</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>
<!--================Header Menu Area =================-->
