
<footer class="footer-area section_gap">
    <div class="container">
        <div class="row">
            <div class="col-lg-4  col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <h6 class="footer_title">THE FRAMEWORK</h6>
                    <p>Bangladesh National Digital Architecture framework is based on leading standards, practices and frameworks like Open Group's TOGAF 9.1 ®
                        but aligned and tailored as per Bangladesh requirements and strategic objectives.</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <h6 class="footer_title">Bangladesh Computer Council</h6>
                    <div class="row">
                        <div class="col-6">
                            <ul class="list">
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Support</a></li>
                                <li><a href="#">Services</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                            </ul>
                        </div>
                        <div class="col-6">
                            <ul class="list">
                                <li><a href="#">Team</a></li>
                                <li><a href="#">We are Hiring</a></li>
                                <li><a href="#">Term & condition</a></li>
                                <li><a href="#">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 ">
                <div class="single-footer-widget">
                    <h6 class="footer_title">Contact info</h6>

                    <address>
                        <p><i class="fa fa-home pr-10"></i> Address: E-14/X, BCC Bhaban</p>
                        <p><i class="fa fa-globe pr-10"></i> Dhaka, Bangladesh </p>
                        <p><i class="fa fa-mobile pr-10"></i> Phone : +8801670974703 </p>
                        <p><i class="fa fa-phone pr-10"></i> Fax : 88-02-9124626 </p>
                        <p><i class="fa fa-envelope pr-10"></i> Email :   <span>bnea@bcc.net.bd</span></p>
                    </address>
                </div>
            </div>

        </div>
        <div class="border_line"></div>
        <div class="row footer-bottom d-flex justify-content-between align-items-center">
            <p class="col-lg-8 col-md-8 footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This site is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="index.php" target="_blank">BNDA team</a>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
            <div class="col-lg-4 col-md-4 footer-social">
                <a href="https://www.facebook.com/bnea.bcc/" target="_blank"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-dribbble"></i></a>
                <a href="#"><i class="fa fa-behance"></i></a>
            </div>
        </div>
    </div>
</footer>