<?php
session_start();
if(!$_SESSION['username'])
{
    header("Location: login.php");//redirect to login page to secure the welcome page without login access.
}

include("../database/db_connection.php");
include '../pages/constants.php';

if(isset($_POST["add"])) {

    $name= $_POST["name"];
    $designation= $_POST["designation"];
    $description= $_POST["description"];
    $order= $_POST["order"];
    $color= $_POST["color"];


    $target_dir = "../uploads/persons/";
    if (!file_exists($target_dir)) {
        mkdir($target_dir, 0777, true);
    }
    $target_file = $target_dir . basename($_FILES["img_file"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

    $check = getimagesize($_FILES["img_file"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "<script>alert('File is not an Image!')</script>";
        $uploadOk = 0;
    }

    // Check file size
    if ($_FILES["img_file"]["size"] > 500000) {
        echo "<script>alert('File is too large!')</script>";
        $uploadOk = 0;
    }

    if ($uploadOk == 0) {
        echo "<script>alert('Sorry, your file was not uploaded!')</script>";
    // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["img_file"]["tmp_name"], $target_file)) {
            echo "The file ". basename( $_FILES["img_file"]["name"]). " has been uploaded.";
            $stmt = $dbcon->prepare("INSERT INTO key_persons_info(name,designation,display_order,image_path,description,color_code) VALUES (?, ?, ?, ?,?,?)");
            $stmt->bind_param('ssisss',$name,$designation, $order, $target_file, $description,$color);
            $stmt->execute();
            $stmt->close();

            echo "<script>window.open('persons-list.php?msg=Info Has been added Successfully','_self')</script>";

        } else {
            echo "<script>alert('Sorry, there was an error uploading your file')</script>";;
        }
    }


    echo "<script>window.open('persons-list.php','_self')</script>";


    $dbcon->close();


}else if(isset($_POST["edit"])) {
    $id= $_POST["id"];
    $name= $_POST["name"];
    $designation= $_POST["designation"];
    $description= $_POST["description"];
    $order= $_POST["order"];
    $color= $_POST["color"];


    $query="select * from key_persons_info where id=".$id;//select query for viewing users.
    $run=$dbcon->query($query);//here run the sql query.
    $row= mysqli_fetch_array($run);
    $image_path=  $row["image_path"];

    if(is_uploaded_file($_FILES['img_file']['tmp_name'])){

        $target_dir = "../uploads/persons/";
        if (!file_exists($target_dir)) {
            mkdir($target_dir, 0777, true);
        }
        $target_file = $target_dir . basename($_FILES["img_file"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        $check = getimagesize($_FILES["img_file"]["tmp_name"]);
        if($check !== false) {
            echo "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            echo "<script>alert('File is not an Image!')</script>";
            $uploadOk = 0;
        }

        // Check file size
        if ($_FILES["img_file"]["size"] > 500000) {
            echo "<script>alert('File is too large!')</script>";
            $uploadOk = 0;
        }

        if ($uploadOk == 0) {
            echo "<script>alert('Sorry, your file was not uploaded!')</script>";
            // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["img_file"]["tmp_name"], $target_file)) {
                echo "The file ". basename( $_FILES["img_file"]["name"]). " has been uploaded.";
                $image_path= $target_file;
            } else {
                echo "<script>alert('Sorry, there was an error uploading your file')</script>";;
            }
        }
    }

    $stmt =  $dbcon->prepare("update key_persons_info set name=?,designation=?,display_order=?,image_path=?,description=?,color_code=? where id=?");
    $stmt->bind_param('ssisssi',$name,$designation, $order, $image_path, $description,$color,$id);

    if($stmt->execute()){
        echo "<script>window.open('persons-list.php?msg=Info Has been Updated Successfully','_self')</script>";
    }
    else{
        echo "<script>window.open('persons-list.php','_self')</script>";
    }

    $stmt->close();
    $dbcon->close();
}






?>