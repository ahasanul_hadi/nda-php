<?php
session_start();
if (!$_SESSION['username']) {
    header("Location: login.php");//redirect to login page to secure the welcome page without login access.
}

include("../database/db_connection.php");
$person_id = $_GET['id'];
$row = null;
if (isset($person_id)) {
    $query = "select * from key_persons_info  WHERE id=" . $person_id;
    $run = mysqli_query($dbcon, $query);
    $row = mysqli_fetch_array($run);
}
$dbcon->close();
$order = $row["display_order"];
$GLOBALS['root'] = "..";
$GLOBALS['project_name'] = "NDA Portal - Admin Panel";
$GLOBALS['menu'] = "new_person";
?>

<html>
<head lang="en">
    <meta charset="UTF-8">

    <!-- main css -->
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/responsive.css">
    <?php include 'includes/css.php'; ?>
    <link href="../vendors/cleditor/jquery.cleditor.css" rel="stylesheet" type="text/css"/>

    <title>Person Edit</title>
</head>


<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <img class="pull-left" style="height: 60px; padding: 5px;" src="<?php echo $GLOBALS['root']; ?>/images/nea-logo.png" alt="">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand"
               href="<?php echo $GLOBALS['root']; ?>/admin/"><?= $GLOBALS['project_name'] ?></a>
        </div>

    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <?php include 'includes/menu.php'; ?>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Edit New Key Person</h1>
            <?php if (isset($_GET['msg'])) { ?>
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Success!</strong><?= $_GET['msg'] ?>
                </div>
            <?php } ?>
            <form role="form" class="form-horizontal" method="post" action="persons-upload.php"
                  enctype="multipart/form-data">


                <div class="row form-group">
                    <label class="control-label col-sm-2">Full Name</label>
                    <div class="col-md-4">
                        <input class="form-control" placeholder="Name" name="name" value="<?php echo $row['name'] ?>">
                    </div>
                </div>
                <div class="row form-group">
                    <label class="control-label col-sm-2">Designation</label>
                    <div class="col-md-4">
                        <input class="form-control" placeholder="Designation" name="designation"
                               value=" <?php echo $row['designation'] ?>">
                    </div>
                </div>
                <div class="row form-group">
                    <label class="control-label col-sm-2">
                        Upload Image
                    </label>
                    <div class="col-md-4 btn btn-pink ">
                        <span style="float: left"> <input class="form-control" type="file" class="img_upload" name="img_file"></span>

                        <span style="float: left" id="img_area"> <img width="80px" height="70"
                                                                      src="<?php echo $row['image_path'] ?>"> </span>

                    </div>
                </div>
                <div class="row form-group">
                    <label class="control-label col-sm-2">Modal Description</label>
                    <div class="col-md-4">
                    <textarea class="form-control" id="editor1" rows="5"
                              name="description"><?php echo $row['description'] ?></textarea>

                    </div>
                </div>

                <div class="row form-group">
                    <label class="control-label col-sm-2">Color Code(Hex)</label>
                    <div class="col-sm-4">
                        <input id="pickedColor" class="form-control" placeholder="#code" name="color"
                               value="<?php echo $row['color_code'] ?>">

                    </div>
                    <div class="col-sm-4">
                        <div id="colorSelector"  >
                            <div class="col-sm-2" style="width: 50px; height: 26px; background-color: <?php echo $row['color_code'] ?>;" ></div>
                            <input class="col-sm-4" type="button" value="Choose Color">
                        </div>
                    </div>
                </div>

                <div class="row form-group">
                    <label class="control-label col-sm-2">Display Order(from left)</label>
                    <div class="col-sm-4">
                        <select class="form-control" name="order">
                            <option value="1" <?php if ($order == 1) echo 'selected' ?> >1</option>
                            <option value="2" <?php if ($order == 2) echo 'selected' ?> >2</option>
                            <option value="3" <?php if ($order == 3) echo 'selected' ?> >3</option>
                            <option value="4" <?php if ($order == 4) echo 'selected' ?> >4</option>
                            <option value="5" <?php if ($order == 5) echo 'selected' ?> >5</option>
                            <option value="6" <?php if ($order == 6) echo 'selected' ?> >6</option>
                        </select>
                    </div>
                </div>
                <hr>
                <div class="row form-group">
                    <label class="control-label col-sm-2"></label>
                    <div class="col-md-4">
                        <input type="hidden" name="edit" value="yes">
                        <input type="hidden" name="id" value="<?php echo $row["id"] ?>">
                        <input class="btn btn-md" type="reset" value="Reset" >
                        <input class="btn btn-md btn-success" type="submit" value="Update" name="submit">
                    </div>
                </div>


            </form>

        </div>
    </div>
</div>

<script src="../vendors/cleditor/jquery.browser.min.js" type="text/javascript"></script>
<script src="../vendors/cleditor/jquery.cleditor.js" type="text/javascript"></script>

<link rel="stylesheet" media="screen" type="text/css" href="../vendors/colorpicker/css/colorpicker.css" />
<script type="text/javascript" src="../vendors/colorpicker/js/colorpicker.js"></script>


<script type="text/javascript">
    $(document).ready(function () {

        $('#colorSelector').ColorPicker({
            color: '#0000ff',
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(500);
                return false;
            },
            onChange: function (hsb, hex, rgb) {
                $('#colorSelector div').css('backgroundColor', '#' + hex);
                $("#pickedColor").val("#"+hex);
            }
        });


        $('.img_upload').on('click', function () {
            $('#img_area').hide();
        });

        var options = {
            width: 480,
            height: 200,
            controls: "bold italic underline strikethrough subscript superscript | font size " +
            "style | color highlight removeformat | bullets numbering | outdent " +
            "indent | alignleft center alignright justify | undo redo | " +
            "rule link image unlink | cut copy paste pastetext | print source"
        };

        //editor 1
        var editor1 = $("#editor1").cleditor(options)[0];

        $("#btnClear").click(function (e) {
            e.preventDefault();
            editor1.focus();
            editor1.clear();
        });

        $("#btnAddImage").click(function () {
            editor1.execCommand("insertimage",
                "http://images.free-extras.com/pics/s/smile-1620.JPG", null, null);
            editor1.focus();
        });

        $("#btnGetHtml").click(function () {
            alert($("#editor1").val());
        });

    });
</script>

</body>

</html>
