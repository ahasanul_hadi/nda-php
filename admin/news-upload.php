<?php
session_start();
if(!$_SESSION['username'])
{
    header("Location: login.php");//redirect to login page to secure the welcome page without login access.
}

include("../database/db_connection.php");

if(isset($_POST["submit"])) {
    $title= $_POST["title"];
    $publish_date= $_POST["publish_date"];
    $news= $_POST["news"];


    $target_dir = "../uploads/news/";
    if (!file_exists($target_dir)) {
        mkdir($target_dir, 0777, true);
    }
    $target_file = $target_dir . basename($_FILES["img_file"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

    $check = getimagesize($_FILES["img_file"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }

    // Check file size
    if ($_FILES["img_file"]["size"] > 500000) {
        echo "Sorry, your file is too large.";
        $uploadOk = 0;
    }

    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
        echo "<script>alert('Sorry, your file was not uploaded.')</script>";
    // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["img_file"]["tmp_name"], $target_file)) {
            echo "The file ". basename( $_FILES["img_file"]["name"]). " has been uploaded.";
        } else {
            echo "<script>alert('Sorry, there was an error uploading your file.')</script>";
            echo "<script>window.open('news-list.php','_self')</script>";
        }
    }


    $stmt = $dbcon->prepare("INSERT INTO news_events(title,publish_date,image_path,description) VALUES (?, ?, ?, ?)");
    $stmt->bind_param('ssss', $title, $publish_date, $target_file, $news);
    $stmt->execute();
    $stmt->close();
    $dbcon->close();

    echo "<script>alert('Successfully Added News!')</script>";
    header("Location: news-list.php?msg=News is uploaded successfully");
}

$dbcon->close();

?>