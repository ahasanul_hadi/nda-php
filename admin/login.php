<?php $GLOBALS['root'] = "./.."; ?>
<?php
session_start();//session starts here  

?>


    <html>
    <head lang="en">
        <meta charset="UTF-8">
        <?php include '../includes/css.php'; ?>
        <!-- main css -->
        <link rel="stylesheet" href="../css/style.css">
        <link rel="stylesheet" href="../css/responsive.css">
        <title>Login</title>
    </head>
    <style>
        .login-panel {
            margin-top: 20px;
        }
    </style>

    <body>


    <div class="container">
        <div class="row">
            <div class="offset-md-4 col-md-4 text-center">
                <img style="height: 120px;" src="<?php echo $GLOBALS['root']; ?>/images/nea-logo.png" alt="">
            </div>
        </div>

        <div class="col-md-4 offset-md-4 wel_item">
            <div class="login-panel panel panel-success">
                <div class="panel-heading ">
                    <h3 class="panel-title text-center">NDA Portal Login</h3><br>
                </div>
            </div>
            <div class="panel-body">

                <form role="form" method="post" action="login.php">
                    <fieldset>
                        <div class="form-group">
                            <input class="form-control" placeholder="Username" name="username" type="username"
                                   autofocus>
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Password" name="pass" type="password" value="">
                        </div>


                        <input class="btn btn-lg btn-success btn-block" type="submit" value="Login" name="login">

                        <!-- Change this to a button or input when using this as a form -->
                        <!--  <a href="index.html" class="btn btn-lg btn-success btn-block">Login</a> -->
                    </fieldset>
                </form>
            </div>
        </div>
    </div>

    </div>


    </body>

    </html>

<?php

include("../database/db_connection.php");

if (isset($_POST['login'])) {
    $user_name = $_POST['username'];
    $user_pass = $_POST['pass'];

    $check_user = "select * from users WHERE user='$user_name'AND password='$user_pass'";

    $run = $dbcon->query($check_user);

    if (mysqli_num_rows($run)) {
        echo "<script>window.open('index.php','_self')</script>";

        $_SESSION['username'] = $user_name;//here session is used and value of $user_name store in $_SESSION.

    } else {
        echo "<script>alert('Username or password is incorrect!')</script>";
    }
}
$dbcon->close();
?>