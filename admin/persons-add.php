<?php
session_start();
if (!$_SESSION['username']) {
    header("Location: login.php");//redirect to login page to secure the welcome page without login access.
}
$GLOBALS['root'] = "..";
$GLOBALS['project_name'] = "NDA Portal - Admin Panel";
$GLOBALS['menu'] = "new_person";
?>

<html>
<head lang="en">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <?php include 'includes/css.php'; ?>
    <link href="../vendors/cleditor/jquery.cleditor.css" rel="stylesheet" type="text/css"/>
    <title>Key Persons upload</title>
</head>


<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <img class="pull-left" style="height: 60px; padding: 5px;" src="<?php echo $GLOBALS['root']; ?>/images/nea-logo.png" alt="">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand"
               href="<?php echo $GLOBALS['root']; ?>/admin/"><?= $GLOBALS['project_name'] ?></a>
        </div>

    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <?php include 'includes/menu.php'; ?>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Add New Key Person</h1>

            <?php if (isset($_GET['msg'])) { ?>
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Success!</strong><?= $_GET['msg'] ?>
                </div>
            <?php } ?>

            <form role="form" class="form-horizontal" method="post" action="persons-upload.php"
                  enctype="multipart/form-data">


                <div class="row form-group">
                    <label class="control-label col-sm-2">Full Name</label>
                    <div class="col-md-4">
                        <input class="form-control" placeholder="Name" name="name" autofocus>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="control-label col-sm-2">Designation</label>
                    <div class="col-md-4">
                        <input class="form-control" placeholder="Designation" name="designation">
                    </div>
                </div>
                <div class="row form-group">
                    <label class="control-label col-sm-2">
                        Upload Image
                    </label>
                    <div class="col-md-4 btn btn-pink ">
                        <span style="float: left"> <input class="form-control" type="file" name="img_file"></span>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="control-label col-sm-2">Modal Description</label>
                    <div class="col-md-4">
                        <textarea class="form-control" id="editor1" rows="5" name="description"></textarea>

                    </div>
                </div>

                <div class="row form-group">
                    <label class="control-label col-sm-2">Color Code(Hex)</label>
                    <div class="col-sm-4">
                        <input class="form-control" placeholder="#code" name="color" id="pickedColor" readonly>
                    </div>
                    <div class="col-sm-4">
                        <div id="colorSelector"  >
                            <div class="col-sm-2" style="width: 50px; height: 26px; background-color: #5100ff;" ></div>
                            <input class="col-sm-4" type="button" value="Choose Color">
                        </div>
                    </div>
                </div>

                <div class="row form-group">
                    <label class="control-label col-sm-2">Display Order(from left)</label>
                    <div class="col-sm-4">
                        <select class="form-control" name="order">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3" selected>3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                        </select>
                    </div>
                </div>
                <hr>

                <div class="row form-group">
                    <label class="control-label col-sm-2"></label>
                    <div class="col-md-4">
                        <input type="hidden" name="add" value="yes">
                        <input class="btn btn-md" type="reset" value="Reset" >
                        <input class="btn btn-md btn-success" type="submit" value="Submit" name="submit">
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>

<script src="../vendors/cleditor/jquery.browser.min.js" type="text/javascript"></script>
<script src="../vendors/cleditor/jquery.cleditor.js" type="text/javascript"></script>
<link rel="stylesheet" media="screen" type="text/css" href="../vendors/colorpicker/css/colorpicker.css" />
<script type="text/javascript" src="../vendors/colorpicker/js/colorpicker.js"></script>


<script type="text/javascript">
    $(document).ready(function () {

        $('#colorSelector').ColorPicker({
            color: '#0000ff',
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(500);
                return false;
            },
            onChange: function (hsb, hex, rgb) {
                $('#colorSelector div').css('backgroundColor', '#' + hex);
                $("#pickedColor").val("#"+hex);
            }
        });

        var options = {
            width: 480,
            height: 200,
            controls: "bold italic underline strikethrough subscript superscript | font size " +
            "style | color highlight removeformat | bullets numbering | outdent " +
            "indent | alignleft center alignright justify | undo redo | " +
            "rule link image unlink | cut copy paste pastetext | print source"
        };

        //editor 1
        var editor1 = $("#editor1").cleditor(options)[0];

        $("#btnClear").click(function (e) {
            e.preventDefault();
            editor1.focus();
            editor1.clear();
        });

        $("#btnAddImage").click(function () {
            editor1.execCommand("insertimage",
                "http://images.free-extras.com/pics/s/smile-1620.JPG", null, null);
            editor1.focus();
        });

        $("#btnGetHtml").click(function () {
            alert($("#editor1").val());
        });

    });
</script>

</body>

</html>
