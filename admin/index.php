<?php
session_start();
if (!$_SESSION['username']) {
    header("Location: login.php");//redirect to login page to secure the welcome page without login access.
}
$GLOBALS['root'] = "..";
$GLOBALS['project_name'] = "NDA Portal - Admin Panel";
$GLOBALS['menu'] = "";
?>

<html>
<head lang="en">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <?php include 'includes/css.php'; ?>
    <!-- main css -->


    <title>Admin Panel</title>
</head>


<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <img class="pull-left" style="height: 60px; padding: 5px;" src="<?php echo $GLOBALS['root']; ?>/images/nea-logo.png" alt="">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo  $GLOBALS['root'];?>/admin/"><?= $GLOBALS['project_name'] ?></a>
        </div>

    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <?php include 'includes/menu.php'; ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1>You are logged in as <?=$_SESSION['username']?></h1>
        </div>
    </div>
</div>
</body>  
  
</html>