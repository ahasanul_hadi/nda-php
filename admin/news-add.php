<?php
session_start();
if (!$_SESSION['username']) {
    header("Location: login.php");//redirect to login page to secure the welcome page without login access.
}
$GLOBALS['root'] = "..";
$GLOBALS['project_name'] = "NDA Portal - Admin Panel";
$GLOBALS['menu'] = "new_news";
?>

<html>
<head lang="en">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <?php include 'includes/css.php'; ?>
    <!-- main css -->
    <link href="../vendors/cleditor/jquery.cleditor.css" rel="stylesheet" type="text/css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css"
          rel="stylesheet">

    <title>News upload</title>
</head>


<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <img class="pull-left" style="height: 60px; padding: 5px;" src="<?php echo $GLOBALS['root']; ?>/images/nea-logo.png" alt="">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand"
               href="<?php echo $GLOBALS['root']; ?>/admin/"><?= $GLOBALS['project_name'] ?></a>
                 </div>

    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <?php include 'includes/menu.php'; ?>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">New News & Event</h1>

            <div class="row placeholders">

                <form role="form" class="form-horizontal" method="post" action="news-upload.php"
                      enctype="multipart/form-data">

                    <div class="row form-group">
                        <label class="control-label col-sm-2">Title</label>
                        <div class="col-md-4">
                            <input class="form-control" placeholder="Title" name="title" autofocus>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="control-label col-sm-2">Publish Date</label>
                        <div class='col-md-4'>
                            <div class='input-group date' id='datetimepicker1'>
                                <input type='text' class="form-control" name="publish_date" autocomplete="off" readonly/>
                                <span class="input-group-addon">
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <label class="control-label col-sm-2">News text</label>
                        <div class="col-md-4">
                            <textarea class="form-control" rows="5" name="news" id="editor1"></textarea>
                        </div>
                    </div>


                    <div class="row form-group">
                        <label class="control-label col-sm-2">
                            Upload Image
                        </label>
                        <div class="col-md-4 btn btn-pink ">
                            <span style="float: left"> <input class="form-control" type="file" name="img_file"></span>
                        </div>
                    </div>

                    <hr>
                    <div class="row form-group">
                        <label class="control-label col-sm-2 "></label>
                        <div class="col-md-4 text-left">
                            <input class="btn btn-md" type="reset" value="Reset">
                            <input class="btn btn-md btn-success" type="submit" value="Submit" name="submit">
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<script src="../vendors/cleditor/jquery.browser.min.js" type="text/javascript"></script>
<script src="../vendors/cleditor/jquery.cleditor.js" type="text/javascript"></script>


<script type="text/javascript">

    $(function () {
        $('#datetimepicker1').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayBtn: true,
            pickerPosition: "bottom-left"
        });



    });


    $(document).ready(function () {


        var options = {
            width: 480,
            height: 200,
            controls: "bold italic underline strikethrough subscript superscript | font size " +
            "style | color highlight removeformat | bullets numbering | outdent " +
            "indent | alignleft center alignright justify | undo redo | " +
            "rule link image unlink | cut copy paste pastetext | print source"
        };

        //editor 1
        var editor1 = $("#editor1").cleditor(options)[0];

        $("#btnClear").click(function (e) {
            e.preventDefault();
            editor1.focus();
            editor1.clear();
        });

        $("#btnAddImage").click(function () {
            editor1.execCommand("insertimage",
                "http://images.free-extras.com/pics/s/smile-1620.JPG", null, null);
            editor1.focus();
        });

        $("#btnGetHtml").click(function () {
            alert($("#editor1").val());
        });

    });
</script>


</body>

</html>
