<!-- Bootstrap core CSS -->
<link href="<?php echo  $GLOBALS['root'];?>/admin/css/bootstrap.min.css" rel="stylesheet">

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<link href="<?php echo  $GLOBALS['root'];?>/admin/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="<?php echo  $GLOBALS['root'];?>/admin/css/dashboard.css" rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<script src="<?php echo  $GLOBALS['root'];?>/admin/css/ie-emulation-modes-warning.js.download"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<script src="<?php echo  $GLOBALS['root'];?>/js/jquery-3.2.1.min.js"></script>
<script src="<?php echo  $GLOBALS['root'];?>/js/popper.js"></script>
<script src="<?php echo  $GLOBALS['root'];?>/js/bootstrap.min.js"></script>