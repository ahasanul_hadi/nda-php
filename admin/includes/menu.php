<?php
/**
 * Created by PhpStorm.
 * User: lict
 * Date: 9/3/2018
 * Time: 4:50 PM
 */
?>
<div class="col-sm-3 col-md-2 sidebar">
    <ul class="nav nav-sidebar">
        <li <?php if($GLOBALS['menu']=='new_news'){ ?> class="active" <?php }?> > <a href="<?php echo  $GLOBALS['root'];?>/admin/news-add.php">Add News & Event <span class="sr-only">(current)</span></a></li>
        <li <?php if($GLOBALS['menu']=='news_list'){ ?> class="active" <?php }?>><a href="<?php echo  $GLOBALS['root'];?>/admin/news-list.php">News List</a></li>
    </ul>

    <ul class="nav nav-sidebar">
        <li <?php if($GLOBALS['menu']=='new_person'){ ?> class="active" <?php }?>><a href="<?php echo  $GLOBALS['root'];?>/admin/persons-add.php"">Add New Key Person <span class="sr-only">(current)</span></a></li>
        <li <?php if($GLOBALS['menu']=='person_list'){ ?> class="active" <?php }?>><a href="<?php echo  $GLOBALS['root'];?>/admin/persons-list.php"">Key Persons </a></li>
    </ul>

    <ul class="nav nav-sidebar">
        <li><a href="<?php echo  $GLOBALS['root'];?>/admin/logout.php"">Log Out (<?=$_SESSION['username'] ?> )</a></li>
    </ul>
</div>