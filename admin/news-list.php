<?php
session_start();
if (!$_SESSION['username']) {
    header("Location: login.php");//redirect to login page to secure the welcome page without login access.
}
$GLOBALS['root'] = "..";
$GLOBALS['project_name'] = "NDA Portal - Admin Panel";
$GLOBALS['menu'] = "news_list";
?>

<html>
<head lang="en">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <?php include 'includes/css.php'; ?>
    <!-- main css -->

    <title>News List</title>
</head>


<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <img class="pull-left" style="height: 60px; padding: 5px;" src="<?php echo $GLOBALS['root']; ?>/images/nea-logo.png" alt="">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand"
               href="<?php echo  $GLOBALS['root'];?>/admin/"><?= $GLOBALS['project_name'] ?></a>
        </div>

    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <?php include 'includes/menu.php'; ?>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header"> News & Event</h1>

            <?php if (isset($_GET['msg'])) { ?>
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Success!</strong><?= $_GET['msg'] ?>
                </div>
            <?php } ?>

            <div class="table-scrol">

                <div class="table-responsive"><!--this is used for responsive display in mobile and other devices-->


                    <table id="table" class="table table-bordered table-hover table-striped">
                        <thead>

                        <tr>

                            <th>Id</th>
                            <th>Title</th>
                            <th>Publish Date</th>
                            <th style="max-width: 20%">News</th>
                            <th>Image</th>
                            <th>Action</th>
                            <th>View</th>
                            <th>Delete News</th>
                        </tr>
                        </thead>

                        <?php
                        include("../database/db_connection.php");
                        $query = "select * from news_events where isDeleted=0 order by publish_date DESC";//select query for viewing users.
                        $run = $dbcon->query($query);//here run the sql query.

                        $id = 0;
                        while ($row = mysqli_fetch_array($run)) {
                            $news_id = $row["id"];
                            $published = ($row["isPublished"] == 1) ? true : false;
                            $title = $row["title"];
                            $publish_date = $row["publish_date"];
                            $description = strip_tags($row["description"]);
                            $part = mb_substr($description, 0, 100) . '...';
                            $image_path = $row["image_path"];


                            $id++;
                            ?>

                            <tr>
                                <!--here showing results in the table -->
                                <td><?php echo $id; ?></td>
                                <td><?php echo $title; ?></td>
                                <td><?php echo $publish_date; ?></td>
                                <td><?php echo $part; ?></td>
                                <td><img width="120px" height="100px" src="<?php echo $image_path; ?>"></td>
                                <td>
                                    <a href="news-process.php?id=<?php echo $news_id ?>&pub=<?php echo $row['isPublished'] ?>">
                                        <button class="btn btn-warning"><?php echo $published == true ? 'unpublish' : 'publish' ?></button>
                                    </a></td>
                                <td><a href="../pages/news-single.php?id=<?php echo $news_id ?>" target="_blank">
                                        <button class="btn btn-success">View</button>
                                    </a></td>
                                <td><a href="news-process.php?del=<?php echo $news_id ?>">
                                        <button class="btn btn-danger">Delete</button>
                                    </a></td>
                            </tr>

                        <?php }
                        $run->close();
                        $dbcon->close();
                        ?>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<link rel="stylesheet" type="text/css" href="../vendors/datatable/datatables.min.css"/>
<script type="text/javascript" src="../vendors/datatable/datatables.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#table').DataTable();
    } );

</script>

</body>

</html>