<?php
$path= $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'NDA';
$path = str_replace("\\", "/", $path);
$GLOBALS['root'] = '../NDA/';
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="images/nea-logo.png" type="image/png">
    <title>National Digital Architecture</title>
    <!-- Bootstrap CSS -->
    <?php include 'includes/css.php';?>
    <!-- main css -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">
    <style>


    </style>

</head>
<body>



<?php include 'skeleton/header.php';?>


<section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
        <div class="container">
            <div class="banner_content text-right">
                <div class="page_link">
                    <a href="#">Home</a>
                    <a href="gallery.html">Standard</a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class=" p_120">
    <div class="container">
        <div class="row feature_inner">
            <div class="col-lg-4">
                <div class="feature_item" style="border: 1px solid #f44a40">
                    <i class="fa fa-clipboard w3-margin-bottom w3-text-theme" style="font-size:120px;color:#DD0000"></i>
                    <p>Application architecture Standards</p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="feature_item" style="border: 1px solid #f44a40">
                    <i class="fa fa-briefcase w3-margin-bottom w3-text-theme" style="font-size:120px;color:#DD0000"></i>
                    <p>Business architecture Standards</p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="feature_item" style="border: 1px solid #f44a40">
                    <i class="fa fa-file-o w3-margin-bottom w3-text-theme" style="font-size:120px;color:#DD0000"></i>
                    <p>Data architecture Standards</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================End Feature Area =================-->



<!--================ start footer Area  =================-->
<?php include "skeleton/footer.php"; ?>
<!--================ End footer Area  =================-->




<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<?php include "includes/js.php"; ?>



</body>
</html>