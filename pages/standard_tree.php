<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link href="../css/dtree.css" rel="stylesheet" />
    <script src="../js/jquery-2.1.1.min.js"></script>
    <!--<script src="../js/jquery-3.2.1.min.js"></script>-->
<script src="../js/dtree.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".dTree").dTree();
    });
</script>
<title>Directory Tree</title>
</head>
<body>
      
	  <div class="container">
	  
	      <div class="row" style="padding-bottom:50px;">
	        <div style="width:30%;float:left;background-color:#121212;border:2px solid #808080">
	        <div style="width:100%;background-color:#f2f2f2;padding:1.6%">
				<span style="color: #000000;font-size:22px; font-family: Helvetica; font-weight: bold;">Architecture Standards</span>
			</div>
	        <div style="width:100%;border-top:2px solid #808080;background-color:#f2f2f2;height:513px;overflow-Y:auto; padding: 5px;;font-size:12px" class="dTree">

					<?php 
					include ('standard_data.php'); 
					$menu = new MenuBuilder();
					echo  $menu->get_menu_html();
					?>
				  
            </div>
		</div>
	    <div style="width:70%;float:left;background-color:#f2f2f2; border: 2px solid #808080; border-left:0px; height: 100%">
			<div style="width:100%;color: #808080;font-size:22px; padding:0.7%; align: middle; border-bottom: 2px solid #808080"><?php  echo base64_decode($_GET['standard']); ?></div>
		
			
			<div id="home" style="display:block" align="center">	
	        <div style="width:100%;height:512px;background-color:#FFFFFF;overflow-Y:auto">
			<?php 
			/*$tableNm='dtree_'.$standard.'_standard'; 
			//$conn = new mysqli('172.18.18.18', 'nea', 'qu9u6usy3', 'nea_db');
			$conn = new mysqli('127.0.0.1', 'root', 'bccnea@2015', 'nea_db');
			$overview = "select uploads,homeFlag from $tableNm where homeFlag!=0";
			$overviewResult = $conn->query($overview);
			$overviewRow = $overviewResult->fetch_assoc();
			echo "<img src='../".$overviewRow['uploads']."' />";*/	
			?>
						
			</div>
			</div>
			<div id="home1"  style="width:100%;height:510px;background-color:#FFFFFF;overflow-Y:auto;display:none">
			<?php
				 for($x=1;$x<$z;$x++) 
				 { 
				 ?>
				<div style="margin:2%" id="<?php echo $x; ?>">
				
				</div>
				<?php } ?>
       		 </div>
	  		</div>
	  		</div>
	  </div>
	</div> 
<input type="hidden" id="chkY" />
</body>
</html>


<script>
function showUser(str,id,tbl) 
{
  var valu=document.getElementById('child'+id).checked;
  var chkY = document.getElementById('chkY').value;
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
		
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) 
			{
				if(valu == true)
				{
				
                document.getElementById(id).innerHTML = xmlhttp.responseText;
				chkY++;
				document.getElementById('chkY').value=chkY;
                if(chkY == 1)
				{
				document.getElementById('home').style.display = 'none';
                document.getElementById('home1').style.display = 'block';
				}
				}
				else
				{
                document.getElementById(id).innerHTML = '';
				chkY--;
				document.getElementById('chkY').value=chkY;
				if(chkY == 0)
				{
                document.getElementById('home1').style.display = 'none';
                document.getElementById('home').style.display = 'block';
				}
				}
				
            }
        }
        xmlhttp.open("GET","standard_child.php?q="+str+"&t="+tbl,true);
        xmlhttp.send();
}
</script>