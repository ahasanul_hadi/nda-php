<?php
$GLOBALS['root'] = "./..";
$news_id=$_GET['id'];
include("../database/db_connection.php");
$query="select * from news_events where id=".$news_id;
$run=$dbcon->query($query);
$row= mysqli_fetch_array($run);
$dbcon->close();
?>



<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="../images/nea-logo.png" type="image/png">
        <title>News</title>
        <!-- Bootstrap CSS -->
        <?php include '../includes/css.php';?>
        <!-- main css -->
        <link rel="stylesheet" href="../css/style.css">
        <link rel="stylesheet" href="../css/responsive.css">

    </head>
    <body>

    <?php include '../skeleton/header.php';?>

    <!--================Home Banner Area =================-->
    <!--================Home Banner Area =================-->
    <section class="banner_area">
        <div class="banner_inner d-flex align-items-center">
            <div class="container">
                <div class="banner_content text-right">
                    <div class="page_link">
                        <a href="../index.php">Home</a>
                        <a href="#">News</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Home Banner Area =================-->
    <!--================End Home Banner Area =================-->

    <!--================Event Details Area =================-->
    <section class="event_details_area p_120">
        <div class="container">
            <div class="event_d_inner">
                <img class="img-fluid" src="<?php echo $row["image_path"]?>" alt="">
                <div class="row event_text_inner">
                    <div class="col-lg-4">
                        <div class="left_text">
                            <ul class="list">
                                <li><a href="#"><i class="lnr lnr-calendar-full"></i><?php echo $row["publish_date"]?></a></li>
                               <!-- <li><a href="#"><i class="lnr lnr-apartment"></i>Rocky beach church</a></li>
                                <li><a href="#"><i class="lnr lnr-location"></i> Los Angeles, USA</a></li>-->
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="right_text">
                            <h4><?php echo $row["title"]?></h4>
                            <p><?php echo $row["description"]?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Event Details Area =================-->


    <!--================ start footer Area  =================-->
    <?php include "../skeleton/footer.php"; ?>
    <!--================ End footer Area  =================-->




    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <?php include "../includes/js.php"; ?>



    </body>
</html>