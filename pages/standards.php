<?php $GLOBALS['root'] = "./.."; ?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="../images/nea-logo.png" type="image/png">
    <title>Standards</title>
    <!-- Bootstrap CSS -->
    <?php include '../includes/css.php';?>
    <!-- main css -->
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/responsive.css">
    <style>


    </style>

</head>
<body>



<?php include '../skeleton/header.php';?>


<!--================Home Banner Area =================-->
<section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
        <div class="container">
            <div class="banner_content text-right">
                <div class="page_link">
                    <a href="../index.php">Home</a>
                    <a href="standards.php">Standards</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================End Home Banner Area =================-->


<style type="text/css">

    .feature_item{
        padding: 0px;
        min-height: 75px;
    }
</style>

<section class=" p_120">
    <div class="container">
        <div class="row feature_inner">


            <?php
            include '../database/db_connection.php';
            //$conn = new mysqli($server, $username, $password, $database);
            $mnt=mysqli_query($dbcon,"select * from dtree_standard") or die(mysqli_error());
            $mnt_clr=mysqli_fetch_array($mnt);
            do
            {
                ?>

                <div class="col-lg-3">
                    <div class="feature_item" style="border: 1px solid #006a4e;min-height: 290px :  padding: 10px; margin: 15px;" >
                        <div style="min-height: 50px;">
                            <p class="nea_red" style="margin: 20px; "><?php echo $mnt_clr['standard'];	?></p>
                        </div>
                        <a href="standard_detail.php?standard=<?php echo base64_encode($mnt_clr['standard']);?>&tblnm=<?php echo base64_encode($mnt_clr['standard_table']);?>">
                            <i class="fa standard_icon <?php echo $mnt_clr['standard_icon'];?> w3-margin-bottom w3-text-theme" ></i>

                        </a>
                    </div>
                </div>


            <?php }while($mnt_clr=mysqli_fetch_assoc($mnt));
            $dbcon->close();
            ?>

            <div class="col-lg-3">
                <div class="feature_item" style="border: 1px solid #006a4e;min-height: 290px :  padding: 10px; margin: 15px;" >
                    <div style="min-height: 50px;">
                        <p class="nea_red" style="margin: 20px;">All Standards</p>
                    </div>
                    <a href="../uploads/all_standards.pdf">
                        <i class="fa fa-book standard_icon w3-margin-bottom w3-text-theme" ></i>

                    </a>
                </div>
            </div>



        </div>
    </div>
</section>






<!--================End Feature Area =================-->



<!--================ start footer Area  =================-->
<?php include "../skeleton/footer.php"; ?>
<!--================ End footer Area  =================-->




<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<?php include "../includes/js.php"; ?>



</body>
</html>