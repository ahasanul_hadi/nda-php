<?php $GLOBALS['root'] = "./.."; ?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="../images/nea-logo.png" type="image/png">
    <title>Standard Details</title>
    <!-- Bootstrap CSS -->
    <?php include '../includes/css.php';?>
    <!--<link href="../css/theme.css" rel="stylesheet" property="stylesheet">-->
    <link rel="stylesheet" href="../css/animate.css" property="stylesheet">

    <!-- main css -->
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/responsive.css">
    <link href="../css/card.css" rel="stylesheet">


</head>
<body>

<?php include '../skeleton/header.php';?>


<!--================Home Banner Area =================-->

<section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
        <div class="container">
            <div class="banner_content text-right">
                <div class="page_link">
                    <a href="../standards2.php">Standard</a>
                    <a href="#"><?php  echo base64_decode($_GET['standard']); ?></a>
                </div>
            </div>
        </div>
    </div>
</section>


<!--================End Home Banner Area =================-->








    <!--container start-->
    <div class="container" >
        <div style="height:1%">&nbsp;</div>
	    <?php include('standard_tree.php'); ?>
	    <div style="height:5%">&nbsp;</div>
    </div>
      <!-- END CLIENTS -->
    <!--container end-->
    <!--================ start footer Area  =================-->
    <?php include "../skeleton/footer.php"; ?>
    <!--================ End footer Area  =================-->


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <?php include "../includes/js.php"; ?>


  </body>
</html>
