<?php $GLOBALS['root'] = "./.."; ?>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="../images/nea-logo.png" type="image/png">
        <title>Contact Us</title>
        <!-- Bootstrap CSS -->
        <?php include '../includes/css.php';?>
        <!-- main css -->
        <link rel="stylesheet" href="../css/style.css">
        <link rel="stylesheet" href="../css/responsive.css">

    </head>
    <body>

    <?php include '../skeleton/header.php';?>


    <!--================Home Banner Area =================-->
    <section class="banner_area">
        <div class="banner_inner d-flex align-items-center">
            <div class="container">
                <div class="banner_content text-right">
                    <div class="page_link">
                        <a href="../index.php">Home</a>
                        <a href="contact.php">Contact</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Home Banner Area =================-->



    <div class="container">
        <h2>Contact Personnel</h2>
            <table class="table table-bordered  table-responsive-sm">
                <tr>
                    <th class="title custom-font text-black"><b>Name</b></th>
                    <th class="title custom-font text-black"><b>Designation</b></th>
                    <th class="title custom-font text-black"><b>Email</b></th>
                    <th class="title custom-font text-black" style="width: 300px;"><b>Phone Number</b></th>
                </tr>
               <!-- <tr>
                    <td class="contact-infos">Mr. Swapan Kumar Sarker</td>
                    <td class="contact-infos">Executive Director, BCC</td>
                    <td class="contact-infos"></td>
                    <td class="contact-infos">+88-02-8144046</td>
                </tr>-->
                <tr>
                    <td class="contact-infos">Mr. Mohammad Rezaul Kkarim</td>
                    <td class="contact-infos">Project Director, BCC</td>
                    <td class="contact-infos">pd.lict@bcc.net.bd</td>
                    <td class="contact-infos">+88-02-8181392, 8181397 Ext-101</td>
                </tr>
                <tr>
                    <td class="contact-infos">Mr. Tarique M Barkatullah</td>
                    <td class="contact-infos">Deputy Project Director, BCC</td>
                    <td class="contact-infos">dpd.lict@bcc.net.bd</td>
                    <td class="contact-infos">+88-02- 8181392, 8181397 Ext-120</td>
                </tr>
                <tr>
                    <td class="contact-infos">Mr. Tanimul Bari</td>
                    <td class="contact-infos">Technical Specialist, LICT</td>
                    <td class="contact-infos">Ts3.lict@bcc.net.bd</td>
                    <td class="contact-infos">+88-02- 8181392,8181397 Ext-102</td>
                </tr>
                <tr>
                    <td class="contact-infos">Ahasanul Hadi</td>
                    <td class="contact-infos">NEA Software Developer, LICT</td>
                    <td class="contact-infos">ahasanul.hadi@gmail.com</td>
                    <td class="contact-infos">01723808031</td>
                </tr>
            </table>
        <br><br>
    </div>

    <div class="container">
        <div class="map-area">
            <!--div class="map-area">
                <div id="googleMap"></div>
            </div-->
            <iframe frameborder="0" height="350" scrolling="no"
                    src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Bangladesh+Computer+Council,+Dhaka&amp;aq=&amp;sll=23.779356,90.374415&amp;sspn=0.011055,0.013797&amp;ie=UTF8&amp;hq=Bangladesh+Computer+Council,&amp;hnear=Dhaka,+Dhaka+Division,+Bangladesh&amp;ll=23.778818,90.374458&amp;spn=0.011055,0.013797&amp;t=m&amp;z=14&amp;iwloc=A&amp;cid=986101539219257989&amp;output=embed"
                    width="100%"></iframe>
        </div>
    </div>
        <!--================Contact Area =================-->
        <section class="contact_area p_120">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="contact_info">
                            <div class="info_item">
                                <i class="lnr lnr-home"></i>
                                <h6>Bangladesh Computer Council</h6>
                                <p>E-14/X, BCC Bhaban, Dhaka, Bangladesh</p>

                            </div>
                            <div class="info_item">
                                <i class="lnr lnr-phone-handset"></i>
                                <h6><a href="#">+8801670974703</a></h6>
                                <p>Sunday - Thursday 9am to 6pm</p>
                            </div>
                            <div class="info_item">
                                <i class="lnr lnr-envelope"></i>
                                <h6><a href="#">Ts3.lict@bcc.net.bd</a></h6>
                                <p>Send us your query anytime!</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <form class="row contact_form" action="contact_process.php" method="post" id="contactForm" novalidate="novalidate">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter your name">
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Enter email address">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="subject" name="subject" placeholder="Enter Subject">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea class="form-control" name="message" id="message" rows="1" placeholder="Enter Message"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12 text-right">
                                <button type="submit" value="submit" class="btn submit_btn">Send Message</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!--================Contact Area =================-->
        
        <!--================ start footer Area  =================-->
    <!--================ start footer Area  =================-->
    <?php include "../skeleton/footer.php"; ?>
    <!--================ End footer Area  =================-->


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <?php include "../includes/js.php"; ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAwuyLRa1uKNtbgx6xAJVmWy-zADgegA2s"></script>
    <script src="<?php echo  $GLOBALS['root'];?>/vendors/clever/map/map-active.js"></script>



    </body>
</html>