<?php $GLOBALS['root'] = "./.."; ?>

<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="../images/nea-logo.png" type="image/png">
        <title>Details | NDA</title>
        <!-- Bootstrap CSS -->
        <?php include '../includes/css.php';?>
        <!-- main css -->
        <link rel="stylesheet" href="../css/style.css">
        <link rel="stylesheet" href="../css/responsive.css">

    </head>
    <body>

		<?php include '../skeleton/header.php';?>


        <!--================Home Banner Area =================-->
        <section class="banner_area">
            <div class="banner_inner d-flex align-items-center">
                <div class="container">
                    <div class="banner_content text-right">
                        <div class="page_link">
                            <a href="../index.php">Home</a>
                            <a href="nea-details.php">NEA Details</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--================End Home Banner Area =================-->

        
        <!--================Welcome Area =================-->
        <section class="welcome_area p_120 ">
        	<div class="container">
        		<div class="row">
					
					<div class="col-lg-7">
        				<div class="welcome_text">
                            <h3>
                                National Enterprise Architecture
                            </h3>
                            <p>
                                The Bangladesh Computer Council (BCC) is one of the apex bodies of the GoB that has been instrumental in carving the path for the development of e-Governance in Bangladesh over the last two decades. At present, the BCC is in the process of determining the potential success of e-Governance in Bangladesh through the establishment of the ‘National Enterprise Architecture and Interoperability Framework’ initiative. This initiative shall be the foundation for the successful ICT adoption in the Government.
                            </p>
                            <p>
                                The transformational potential of technology, especially with reference to the delivery of Government services can only be realized when the efforts towards its adoption, management and subsequent implementation are synchronized among different arms of the Government. A robust enterprise architecture and interoperability framework shall result in the realization of the vision as outlined in ‘Digital Bangladesh’ vision with ICT and emerging technologies.
                            </p>

        				</div>
        			</div>
					
                    <div class="col-lg-5">
                        <div class="welcome_img">
                            <img class="img-fluid text-center" src="../images/img-1.png" alt="">
                        </div>
                    </div>

        			


        		</div>
        	</div>
        </section>
        <!--================End Welcome Area =================-->


        <section class="welcome_area p_120 card">
            <div class="text-center feature-head">
                <h1>
                    National Digital Architecture Components
                </h1>
            </div>
            <div class="container mt-25">
				<div class="row">
					<div class="col-lg-6 col-sm-6">
						<div class="content">
							<h3 class="title nea_green">
                                <i class="fa fa-desktop nea_red" > </i> National Digital Architecture
							</h3>
							<p class="card bg-light p-3">
								Establishment of the contours and the broad structure for whole-of-government EA framework
							</p>
						</div>
					</div>
					<div class="col-lg-6 col-sm-6">
						<div class="content">
							<h3 class="title nea_green">
                                <i class="fa fa-gift nea_red"> </i>	eGovernment Interoperability Framework
							</h3>
                            <p class="card bg-light p-3">
								Design, development and implementation of interoperability framework  across the GoB
							</p>
						</div>
					</div>
			  
					<div class="col-lg-6 col-sm-6">
						<div class="content">
							<h3 class="title nea_green">
                                <i class="fa fa-code nea_red"> </i> Mobile Service Delivery Platform
							</h3>
                            <p class="card bg-light p-3">
								Preparation of the architecture and standards of MSDP
							</p>
						</div>
					</div>
					<div class="col-lg-6 col-sm-6">
						<div class="content">
							<h3 class="title nea_green">
                                <i class="fa fa-book nea_red"> </i> National e-Service Bus
							</h3>
                            <p class="card bg-light p-3">
								Development of a middleware application/platform  for e-service integration
							</p>
						</div>
					</div>
			   
					<div class="col-lg-6 col-sm-6">
						<div class="content">
							<h3 class="title nea_green">
                                <i class="fa fa-bullhorn nea_red"> </i> Capacity Building and Change Management
							</h3>
                            <p class="card bg-light p-3">
								Delineation of broad guidelines for establishing enabling smart e-governance organization for capacity development within government
							</p>
						</div>
					</div>
				</div>
			</div>
        </div>

        <!--================ start footer Area  =================-->
        <?php include "../skeleton/footer.php"; ?>
		<!--================ End footer Area  =================-->
        

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <?php include "../includes/js.php"; ?>

    </body>
</html>