<?php Header ("Content-type: text/css; charset=utf-8");?>

<?php 
$child=$_GET['q'];
$tableNm=$_GET['t'];
include '../database/db_connection.php';
$mnt=mysqli_query($dbcon,"select * from $tableNm where parent_node='$child'") or die(mysqli_error());
$mnt_clr=mysqli_fetch_array($mnt);

?>

<div style="border:2px solid #CCCCCC;">
	<div style="border-bottom:2px solid #CCCCCC;padding-top:0px" align="center"><h4><?php echo $mnt_clr['node_name']; ?></h4></div>
	<div align="center" style="margin:2%;font-size:0.7em">
	<table class="w3-table w3-bordered w3-striped">
	<tr>
	<th width="20%"><?php if($tableNm=='dtree_mobileservice_standards' || $tableNm=='dtree_egif_standards') {echo 'Standard'; } else { echo 'Standard id'; } ?></th>
	<?php if($tableNm=='dtree_applicationarchitecture_standards' || $tableNm=='dtree_egif_standards') { } else {?><th width="25%">Component</th><?php } ?>
	<th width="45%"><?php if($tableNm=='dtree_applicationarchitecture_standards' || $tableNm=='dtree_egif_standards') {echo 'Standard'; } else { echo 'Description'; } ?></th>
	<?php if($tableNm=='dtree_security_standards') { } else {?><th width="10%"><?php if($tableNm=='dtree_mobileservice_standards') {echo 'Reference'; } else { echo 'Clasification'; } ?></th><?php } ?>
	</tr>
<?php do { ?>

		<tr>
		<td><?php echo $mnt_clr['standard_id'];	?></td>
		<?php if($tableNm=='dtree_applicationarchitecture_standards' || $tableNm=='dtree_egif_standards') { } else {?><td><?php echo $mnt_clr['component'];	?></td><?php } ?>

		<td><?php echo $mnt_clr['description'];	?></td>
		<?php if($tableNm=='dtree_security_standards') { } else {?><td><?php echo $mnt_clr['clasification'];	?></td><?php } ?>
	</tr>
	
	
<?php }while($mnt_clr=mysqli_fetch_assoc($mnt));$dbcon->close(); ?>
</table>
</div>
</div>
