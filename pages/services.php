<?php $GLOBALS['root'] = "./.."; ?>

<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="../images/nea-logo.png" type="image/png">
        <title>E-Service</title>
        <!-- Bootstrap CSS -->
        <?php include '../includes/css.php';?>
        <!-- main css -->
        <link rel="stylesheet" href="../css/style.css">
        <link rel="stylesheet" href="../css/responsive.css">

    </head>
    <body>

    <?php include '../skeleton/header.php';?>

    <!--================Home Banner Area =================-->
    <!--================Home Banner Area =================-->
    <section class="banner_area">
        <div class="banner_inner d-flex align-items-center">
            <div class="container">
                <div class="banner_content text-right">
                    <div class="page_link">
                        <a href="../index.php">Home</a>
                        <a href="services.php">E-Service</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Home Banner Area =================-->
    <!--================End Home Banner Area =================-->

    <!--container start-->
    <div class="container">
        <div class="row" style="height: 20px"></div>

        <div class="row table" >

            <div class="col-lg-12 col-sm-12 ">
                <section class="e-services" >
                    <p>
                        E-Services are a critical component for establishing e-Government. All government entities under the Digital Bangladesh vision are currently engaged in provided electronic services or e-services to the citizens and business.  E-Services are the final outcome from BNEA (Bangladesh national Enterprise Architecture). The BNEA framework, principles, standards, guidelines, software, tools and infrastructure would be used to develop better, improved, accessible and reusable e-Services by government organizations. E-Services would include government-to-government (G2G) or government-to-citizen (G2C) or government-to-business (G2B) or government-to-employee (G2E).
                    </p>
                    <p>
                        The Government under BNEA has introduced National Enterprise Architecture (NEA) Bus (known as National e-Service Bus) under Bangladesh National Enterprise Architecture (BNEA) framework to ensure interoperability, availability and reusability of government online services, information and data. National e-Service Bus is software driven middleware platform which is being developed keeping a provision to enable online services, sharing of information and data of ministries, departments and directorates to ensure interoperability and end user’s easy access to it.
                    </p>
                    <div class="row">
                        <div class="col-lg-2 col-sm-2"></div>
                        <div class="col-lg-8 col-sm-8">
                            <h4>E-service Bus and Different Govt applications/services</h4>
                            <img src="../images/neabus.png" class="img-responsive">
                        </div>
                        <div class="col-lg-2 col-sm-2"></div>
                    </div>

                    <p>
                        NEA bus is a critical component in the interoperability architecture domain and key to ensuring seamless exchange of information and services for Government of Bangladesh. Considering the scope and span of NEA bus, due procedures and guidelines have been established around the governance to ensure robust and standardized controls are being followed by the NEA working team. NEA bus is based on service oriented architecture paradigm. The service definition and deployment lifecycle has been covered and due procedures including the roles and responsibilities defined for the benefit of the SOA team working on NEA bus.
                        As a part of BNEA phase 1 conceptualization, couple of e-services have been developed on the BNEA components (NEA bus, service delivery platform and national data center cloud). The individual software components comprising of these e-services would be made available to other government organizations to enable faster software development time and reduce development costs.  E-services that are already integrated with e-service bus:
                    <ul>
                        <li>e-Pension for Directorate of Primary and Mass Education</li>
                        <li>Food procurement system for DG Food office</li>
                        <li>Online Recruitment System for ICT Division</li>
                        <li>NID verification service</li>
                        <li>Govt. Employee verification service</li>
                        <li>Alapon Messenger App</li>
                        <li>Geospatial Data service</li>
                    </ul>
                    </p>
                    <p>
                        More applications and services are in pipeline to be integrated with national e-service bus shortly.
                        Each of these three e-Services share common platforms and technologies:
                    <ul>
                        <li>BNEA design and standards have been used to build the foundation architecture for the three E-services</li>
                        <li>All of the e-services are registered on the NEA Bus </li>
                        <li>The three e-Services uses common technology and programming languages</li>
                        <li>They are using the same business automation and workflow engine</li>
                        <li>Common reporting engine has been used to meet the backend reporting requirements</li>
                        <li>They are hosted on shared cloud infrastructure (BCC national data center</li>
                    </ul>
                    </p>
                    <p>
                        For example, citizen authentication is a foundation building block used by all these three e-Services. A citizen is required to be authenticated against NID database for job application, a farmer needs to be authenticated to be eligible for selling grains to government and teachers are required to establish their identity through national identity. Hence all the three e-services require authentication of a citizen against the NID database.
                    </p>
                </section>
            </div>
            <div class="col-lg-2 col-sm-2 "></div>
        </div>
    </div>
    <!--container end-->


    <!--================ start footer Area  =================-->
    <?php include "../skeleton/footer.php"; ?>
    <!--================ End footer Area  =================-->




    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <?php include "../includes/js.php"; ?>



    </body>
</html>