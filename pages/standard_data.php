
<?php

class MenuBuilder
{
    /**
     * MySQL connection
     */
    var $conn;

    /**
     * Menu items
     */
    var $items = array();

    /**
     * HTML contents
     */
    var $html  = array();


    /**
     * Create MySQL connection
     */

    function MenuBuilder()
    {
        include '../database/db_connection.php';

        //$this->conn =  mysqli_connect('172.18.18.18', 'nea', 'qu9u6usy3','nea_db') or die(mysqli_error('Cannot connect to db'));
		//$this->conn =  mysqli_connect('127.0.0.1', 'root', '', 'nea_db') or die(mysqli_error('Cannot connect to db'));
        $this->conn= $dbcon;
    }

   /* function MenuBuilder( $server,  $username,  $password, $database)
    {

        //$this->conn =  mysqli_connect('172.18.18.18', 'nea', 'qu9u6usy3','nea_db') or die(mysqli_error('Cannot connect to db'));
        $this->conn =  mysqli_connect($server, $username, $password, $database) or die(mysqli_error('Cannot connect to db'));
    }*/

    /**
     * Perform MySQL query and return all results
     */
    function fetch_assoc_all( $sql )
    {
        $result = mysqli_query( $this->conn ,$sql );

        if ( !$result )
            return false;

        $assoc_all = array();

        while( $fetch = mysqli_fetch_assoc( $result ) )
            $assoc_all[] = $fetch;

        mysqli_free_result( $result );

        return $assoc_all;
    }

    /**
     * Get all menu items from database
     */
    function get_menu_items()
    {

        // Change the field names and the table name in the query below to match tour needs
		$sql = 'SELECT * FROM '.base64_decode($_GET['tblnm']).' where parent_node=0 ORDER BY parent_id;';
		
        return $this->fetch_assoc_all( $sql );
    }

    /**
     * Build the HTML for the menu 
     */
    function get_menu_html( $root_id = 0 )
    {
        $this->html  = array();
        $this->items = $this->get_menu_items();
				$i=1;

		$standard=$_GET['standard'];
        foreach ( $this->items as $item )
            $children[$item['parent_id']][] = $item;

        // loop will be false if the root has no children (i.e., an empty menu!)
        $loop = !empty( $children[$root_id] );

        // initializing $parent as the root
        $parent = $root_id;
        $parent_stack = array();

        // HTML wrapper for the menu (open)
        $this->html[] = '<ul>';
        $this->html[] = '<li><b>For '.base64_decode($standard).'</b></li>';

        while ( $loop && ( ( $option = each( $children[$parent] ) ) || ( $parent > $root_id ) ) )
        {
            if ( $option === false )
            {
                $parent = array_pop( $parent_stack );

                // HTML for menu item containing childrens (close)
                $this->html[] = str_repeat( "\t", ( count( $parent_stack ) + 1 ) * 2 ) . '</ul>';
                $this->html[] = str_repeat( "\t", ( count( $parent_stack ) + 1 ) * 2 - 1 ) . '</li>';
            }
            elseif ( !empty( $children[$option['value']['id']] ) )
            {
                $tab = str_repeat( "\t", ( count( $parent_stack ) + 1 ) * 2 - 1 );

                // HTML for menu item containing childrens (open)
                $this->html[] = sprintf(
                    '%1$s<li><a href="%2$s">%3$s</a>',
                    '&nbsp;',   // %1$s = tabulation
                    '#',   // %2$s = link (URL)
                    $option['value']['node_name']  // %3$s = name
                ); 
                $this->html[] = $tab . "\t" . '<ul>';

                array_push( $parent_stack, $option['value']['parent_id'] );
                $parent = $option['value']['id'];
            }
            else
                // HTML for menu item with no children (aka "leaf")

                $this->html[] = sprintf(
                    '%1$s<li><input type="checkbox" id="%4$s" onClick="%5$s">%3$s</li>',
                    //str_repeat( "\t", ( count( $parent_stack ) + 1 ) * 2 - 1 ),   
		    '&nbsp;', // %1$s = tabulation
                    '#',   // %2$s = link (URL)
                    $option['value']['node_name'],   // %3$s = name
		    "child".$i, // %4$s = id
		    "showUser(".$option['value']['id'].",".$i++.",'".base64_decode($_GET['tblnm'])."')" //%5$s = onclick
                );
        }

        // HTML wrapper for the menu (close)
        $this->html[] = '</ul>';
		
		$GLOBALS['z']=$i;
        return implode( "\r\n", $this->html );

    }
}



?>

