<?php $GLOBALS['root'] = "./.."; ?>


<?php session_start();

if(isset($_POST['submit'])){
    $errors= array();
    $file_name = $_FILES['image']['name'];
    $file_size =$_FILES['image']['size'];
    $file_tmp =$_FILES['image']['tmp_name'];
    $file_type=$_FILES['image']['type'];
    $file_ext=strtolower(end(explode('.',$_FILES['image']['name'])));

    $expensions= array("xls");
    if(in_array($file_ext,$expensions)=== false){
        $errors[]="extension not allowed, please choose a xls file.";
    }
    if($file_size > 2097152){
        $errors[]='File size must be excately 2 MB';
    }
    if(empty($errors)==true){
        move_uploaded_file($file_tmp,"../uploads/excel/".substr(md5(uniqid()),8,5).'.'.$file_ext);

    }else{
        print_r($errors);
    }
}
?>

<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="../images/nea-logo.png" type="image/png">
        <title>Maturity Assessment</title>
        <!-- Bootstrap CSS -->
        <?php include '../includes/css.php';?>
        <!-- main css -->
        <link rel="stylesheet" href="../css/style.css">
        <link rel="stylesheet" href="../css/responsive.css">


        <style>

            .Nav-btn
            {
                border:none;
                display:inline-block;
                outline:0;
                padding:9px 16px;
                vertical-align:middle;
                overflow:hidden;
                text-decoration:none !important;
                color:#fff;
                text-align:center;
                transition:.2s ease-out;
                cursor:pointer;
                white-space:nowrap;
                width: 135px;
                margin-right: 5px;
                margin-top:5px;
                font-size:20px;
            }

            .Nav-btn a:hover{background-color:#ccc; text-decoration:none;}

            .bg {
                background: url(../img/maturity-background.jpg) no-repeat center center;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
                height:90%;
            }

            .black_overlay{
                display: none;
                position: absolute;
                top: 0%;
                left: 0%;
                width: 100%;
                height: 200%;
                background-color: gray;
                z-index:1001;
                -moz-opacity: 0.8;
                opacity:.80;
                filter: alpha(opacity=80);
            }
            .white_content {
                display: none;
                position: absolute;
                top: 40%;
                left: 29%;
                width: 30%;
                height:auto;
                max-height:70%;
                padding: 8px;
                box-shadow:inset 0 0 4px 1px #999;
                background-color: white;
                border: 5px solid #6393C1;
                z-index:1002;
                overflow: auto;
                border-radius:10px;
                margin-left:8%;
            }
        </style>

    </head>
    <body>

    <?php include '../skeleton/header.php';?>


    <!--================Home Banner Area =================-->
    <section class="banner_area">
        <div class="banner_inner d-flex align-items-center">
            <div class="container">
                <div class="banner_content text-right">
                    <div class="page_link">
                        <a href="../index.php">Home</a>
                        <a href="assessment.php">Maturity Assessment</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Home Banner Area =================-->


    <div class="container ">
        <div class="row">
            <div class="col-md-6">
                <a href="http://128.199.179.70/bngimg/MaturityTool" class="Nav-btn" target="_BLANK" style="width:100%;background-color:#ed7d31">Online Assessment</a>
                <a href="../uploads/NDA_maturity_assessment_worksheet.xls" class="Nav-btn" style="width:100%;background-color:#f42a41">Download Assessment</a>
                <br/>
                <a href="#" onClick="return openlightbox()" class="Nav-btn" style="background-color:#006a4e;width:100%">Upload Assessment</a>
                
				<div align="center" style="font-size:20px; font-weight:bold; border:1px solid  #777777; margin: 10px 0;padding: 5px;">
                    e-Government
                    <br/>
                    Maturity Asessment
                </div>
			
                <div align="center" style="font-size:14px;font-weight:bold; border:1px solid  #777777; margin: 10px 0;padding: 5px;">
                    Establishing National Digital Architecture and
                    <br/>
                    Interoperability Framework
                </div>
            </div>
            <div class="col-md-6">
                <div style="width:100%;background-color:#ed7d31;padding:5%;border:1px solid  #777777;margin-bottom:5%;border-radius: 3px;">
                    <b>e-Government</b><br/>
                    Web presence, interaction, transaction, integration,<br/>
                    transformation

                </div>
                <div style="width:100%;background-color:#fcf9f9;padding:5%;border:1px solid  #777777;margin-bottom:5%;border-radius: 3px;">
                    <b>Architecture elements</b><br/>
                    Process, development, business linkages, senior <br/>
                    management participation, operating unit <br/>
                    participation, architecture communication, IT security, <br/>
                    architecture governance,  IT investment and strategy,<br/>
                    interoperability, mobility, reusability
                </div>
                <div style="width:100%;background-color:#70ad47;padding:5%;border:1px solid  #777777; border-radius: 3px">
                    <b>Architecture domain</b><br/>
                    Business, data, application, technology, security,<br/>
                    mobility, interoperability


                </div>
            </div>
        </div>
    </div>



    <div id="light" class="white_content">
        <a href = "javascript:void(0)" onclick = "closelightbox()" style="float:right; vertical-align:top; ">
            <img src="../img/icon_cancel.gif" /></a>
        <form action="" method="POST" enctype="multipart/form-data">
            <input type="file" name="image" />
            <input type="submit" name="submit"/>
        </form>
    </div>
    <div id="fade" class="black_overlay"></div>

    <br>

    <!--================ start footer Area  =================-->
    <?php include "../skeleton/footer.php"; ?>
    <!--================ End footer Area  =================-->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <?php include "../includes/js.php"; ?>

    <script>
        function openlightbox()
        {
            document.getElementById('light').style.display='block';
            document.getElementById('fade').style.display='block';
        }
        function closelightbox()
        {
            document.getElementById('light').style.display='none';
            document.getElementById('fade').style.display='none';
        }

    </script>

    </body>
</html>