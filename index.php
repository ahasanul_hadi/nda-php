<?php
$array = explode('/',dirname($_SERVER["SCRIPT_FILENAME"]));
$count = count($array);
$project_name= $array[$count-1];
$GLOBALS['root'] = "/".$project_name;

?>

<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="<?php echo  $GLOBALS['root'];?>/images/favicon.ico" type="image/png">
        <title>National Digital Architecture</title>
        <!-- Bootstrap CSS -->
        <?php include 'includes/css.php';?>
        <!-- main css -->
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/responsive.css">


        <link href='immersive-slider/immersive-slider.css' rel='stylesheet' type='text/css'>

    <style>
        a {
            color: hotpink;
        }

    </style>

    </head>
    <body>



		<?php include 'skeleton/header.php';?>
        
        <!--================Home Banner Area =================-->
        <!--section class="home_banner_area">
            <div class="banner_inner d-flex align-items-center">
            	<div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
				<div class="container">
					<div class="banner_content text-center">
                        <h2>welcome to BCC - NDA Portal</h2>
					</div>
				</div>
            </div>
        </section-->
        <section class=" main home_banner_area">
            <div class="container">
                <div id="immersive_slider">
                    <div class="slide" data-blurred="immersive-slider/img/slide1_blurred.jpg">
                        <div class="content">
                            <h2><a href="pages/nea-details.php" target="_blank">Why National Digital Architecture?</a></h2>
                            <p>Coherent collection of policies, standards, and guidelines to guide government agencies in the design, acquire implement, and manage ICTs.</p>
                            <p><a href="pages/nea-details.php" target="_blank" class="genric-btn success circle">More</a></p>
                        </div>
                        <div class="image">
                            <a href="pages/nea-details.php" target="_blank">
                                <img src="images/carousel/carousel-1.png" alt="Slider 1">
                            </a>
                        </div>
                    </div>
                    <div class="slide" data-blurred="immersive-slider/img/slide2_blurred.jpg">
                        <div class="content">
                            <h2>Governance Framework </h2>
                            <p>Governance will ensure sustainability and maturity improvements.</p>
                        </div>
                        <div class="image">
                             <img src="images/carousel/carousel-2.png" alt="Slider 1">
                        </div>
                    </div>
                    <div class="slide" data-blurred="immersive-slider/img/slide3_blurred.jpg">
                        <div class="content">
                            <h2>Standards and Guidelines</h2>
                            <p>Standards and guidelines will enable better coordination and interoperability.</p>
                        </div>
                        <div class="image">
                            <img src="images/carousel/carousel-3.png" alt="Slider 1">
                        </div>
                    </div>

                    <a href="#" class="is-prev">&laquo;</a>
                    <a href="#" class="is-next">&raquo;</a>
                </div>
            </div>
            <!--================Key persons Area =================-->
            <?php include "key-person/persons.php" ?>

        </section>


        </section>
        <!--================End Home Banner Area =================-->


        <!--================Feature Area =================-->
        <section class="feature_area p_120">
            <div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
            <div class="main_title white_title">
                <h2>Welcome BNDA Portal</h2>
            </div>
            <div class="container">
                <div class="row feature_inner">
                    <div class="col-lg-4">
                        <div class="feature_item">
                            <i class="lnr lnr-diamond"></i>
                            <h4>Strategic Alignment</h4>
                            <p>Enables higher performance by optimizing the contributions of people, process and technology.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="feature_item">
                            <i class="lnr lnr-coffee-cup"></i>
                            <h4>Process Optimization</h4>
                            <p>Looking inward within the business organization and supporting application space.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="feature_item">
                            <i class="lnr lnr-wheelchair"></i>
                            <h4>Cost reduction</h4>
                            <p>Focus on technology standardization, efficiencies, skill leverage and potential retirement  high cost system.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--================End Feature Area =================-->



        <!--================Welcome Area =================-->
        <section class="welcome_area p_120 ">
        	<div class="container">
                <div class="main_title ">
                    <h2>Latest Activities</h2>
                    <p>NATIONAL DIGITAL ARCHITECTURE AND INTEROPERABILITY FRAMEWORK FOR GOVERNMENT OF BANGLADESH</p>
                </div>
        		<div class="row welcome_inner">
        			<div class="col-lg-8">
        				<div class="welcome_text">
                            <p style="margin-bottom: 10px;">
                                <i class="fa fa-check fa-lg pr-10">
                                </i>
                                Selection of three e-Services (ePension, Sim Registration and Food Procurement System) for implementation
                            </p>
                            <p style="margin-bottom: 10px;">
                                <i class="fa fa-check fa-lg pr-10">
                                </i>
                                Draft NDA Reports submitted
                            </p>
                            <p style="margin-bottom: 10px;">
                                <i class="fa fa-check fa-lg pr-10">
                                </i>
                                Session to be held for senior leadership in Bangladesh
                            </p>
                            <br>
                            <a href="pages/nea-details.php" class="genric-btn success circle">
                                Details
                            </a>
        				</div>
        			</div>
        			<div class="mt-10 col-lg-4 d-none d-sm-block">
        				<div class="well">
        					<img class="img-thumbnail" src="images/img-1.png" alt="" style="height: 200px ;width: 300px; ">
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End Welcome Area =================-->

        <hr>

        <!--================Event Area =================-->

        <?php
        include("./database/db_connection.php");
        $query="select * from news_events where isDeleted=0 and isPublished=1 order by publish_date DESC";//select query for viewing users.
        $run=$dbcon->query($query);//here run the sql query.
        ?>

        <section class="event_area p_120 area_background" >
        	<div class="container">
        		<div class="main_title">
        			<h2>News & Events</h2>
        		</div>

        		<div class="event_slider owl-carousel">

                    <?php
                    while($row= mysqli_fetch_array($run))
                    {
                        $news_id=$row["id"];
                        $title=$row["title"];
                        $publish_date=$row["publish_date"];
                        $description=strip_tags($row["description"]);
                        $desc_part= mb_substr($description, 0, 100).'...';
                        $image_path=$row["image_path"];
                    ?>
                        <div class="item">
                            <div class="event_item">
                                <div class="media">
                                    <div class="col-md-7 d-flex" style="display: inline-block;">
                                        <a href="./pages/news-single.php?id= <?php echo $news_id?>" target="_blank">
                                            <img   src="<?php echo './temp/'.$image_path?>" width="163" height="220" alt="">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <?php echo $publish_date?>
                                        <a href="./pages/news-single.php?id= <?php echo $news_id?>" target="_blank"><h4><?php echo $title?></h4></a>
                                        <p><?php echo $desc_part?></p>
                                        <a href="./pages/news-single.php?id= <?php echo $news_id?>" target="_blank"> <h6> More</h6></a>

                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php }
                    $run->close();
                    $dbcon->close();
                    ?>

        		</div>
        	</div>
        </section>
        <!--================End Event Area =================-->
        <hr>
        <!--================Testimonials Area =================-->
        <section class="testimonials_area p_120">
        	<div class="container">
                <div class="main_title">
                    <h2>Testimonial</h2>
                </div>
        		<div class="row testimonials_inner">

        			<div class="col-lg-12">
        				<div class="testi_slider owl-carousel">
							<div class="item">
								<div class="testi_item">
									<img height="120" class="img-responsive " style="border-radius: 50%" src="images/testimonials/ED.png" alt="">
									<p>True eGovernance may be achieved through efficient and effective use of ICT capabilities in execution of Government functions which constitute Governance</p>
									<h4>Swapan Kumar Sarker</h4>
									<h6>Executive Director, BCC</h6>
								</div>
							</div>
							<div class="item">
								<div class="testi_item">
									<img height="120" class="img-responsive " style="border-radius: 50%" src="images/testimonials/person-2.png" alt="">
									<p>The National Digital Architecture will help in better alignment of strategic governance and the information technology utilized to support the strategy leading to greater scalability, increased flexibility in meeting the ever increasing demands on government</p>
									<h4>Md. Rezaul Karim</h4>
									<h6>Project Director, LICT</h6>
								</div>
							</div>
							<div class="item">
								<div class="testi_item">
									<img height="120" class="img-responsive " style="border-radius: 50%" src="images/testimonials/person-3.png" alt="">
									<p>The National Digital Architecture will help to consolidate existing ICT infrastructure and reduce complexity in existing ICT infrastructure.
                                         It will provide the flexibility and capability to make decisions to make, buy, or out-source IT solutions,
                                        thus reducing the overall risk in new investment and the costs of IT ownership</p>
									<h4>Tarique M Barkatullah</h4>
									<h6>Deputy Project Director, LICT</h6>
								</div>
							</div>
						</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End Testimonials Area =================-->
        <hr>




        <!--================IN CASE YOU NEED ANY HELP Area =================-->
        <!--section class="feature_area p_120">
            <div class="overlay bg-parallax" data-stellar-ratio="0.5" data-stellar-vertical-offset="0" data-background=""></div>
            <div class="main_title white_title">
                <h2>IN CASE YOU NEED ANY HELP</h2>
            </div>
            <div class="container">
                <div class="row feature_inner">
                    <div class="col-lg-4">
                        <div class="feature_item">
                            <i class="lnr lnr-diamond"></i>
                            <h4>Access Reference Models</h4>
                            <p>Standards, implementation guidelines, architecture maturity model, reference models are designed for all the domains</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="feature_item">
                            <i class="lnr lnr-coffee-cup"></i>
                            <h4>Frequently Asked Questions</h4>
                            <p>Please visit FAQs section to understand the finer nuances of NDA and understand how to implement it for your Ministry or Department.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="feature_item">
                            <i class="lnr lnr-wheelchair"></i>
                            <h4>CHECK FORUM</h4>
                            <p>Discuss new developments, thoughts and queries on implementation of NDA and its core domains.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
          <hr-->
        <!--================IN CASE YOU NEED ANY HELP Area =================-->




        
        <!--================Start Key users area =================-->
        <section class="p_120 clients_logo_area area_background">
        	<div class="container">

				<div class="main_title">
					<h2>Key Users</h2>
				</div>

				<div class="clients_slider owl-carousel">
					<div class="item">
						<a href="http://www.a2i.pmo.gov.bd/" target="_blank"> <img src="images/clients/logo1.png" alt=""></a>
					</div>
					<div class="item">
						<a href="http://www.nidw.gov.bd/" target="_blank"> <img src="images/clients/logo2.png" alt=""></a>
					</div>
					<div class="item">
						<a href="http://bangladesh.gov.bd/" target="_blank">  <img src="images/clients/logo3.png" alt=""></a>
					</div>
					<div class="item">
						<a href="http://www.bcc.gov.bd/" target="_blank"><img src="images/clients/logo4.png" alt=""></a>
					</div>

				</div>

        	</div>
        </section>




        <!--================End Clients Logo Area =================-->


        <!--================ start Modal for Key persons  =================-->
        <?php include "key-person/person-modal.php"; ?>
        <!--================ End Modal for Key persons  =================-->

        
        <!--================ start footer Area  =================-->
        <?php include "skeleton/footer.php"; ?>
		<!--================ End footer Area  =================-->
        
        
        
        
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <?php include "includes/js.php"; ?>
        <script type="text/javascript" src="immersive-slider/jquery.immersive-slider.js"></script>
        <script type="text/javascript">
            $(document).ready( function() {
                $("#immersive_slider").immersive_slider({
                    container: ".main"
                });
            });

        </script>

    </body>
</html>